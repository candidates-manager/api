import { Resolver, Query } from '@nestjs/graphql';

@Resolver(() => String)
export default class AppResolver {
  constructor() {}
  @Query(() => String)
  async sayHello() {
    return 'Hello World';
  }
}
