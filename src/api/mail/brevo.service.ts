//Imports
import handleError from '@/application/errors/handleError';
import { Injectable } from '@nestjs/common';
//Entities
import { MailEntity } from 'talenthive-models/lib/Entities';

@Injectable()
export default class BrevoService {
    constructor() {}

    async send(mailBody: MailEntity): Promise<true | Error> {
        try {
            //Email Configuration
            const SibApiV3Sdk = require('sib-api-v3-sdk');
            let defaultClient = SibApiV3Sdk.ApiClient.instance;

            let apiKey = defaultClient.authentications['api-key'];
            apiKey.apiKey = process.env.BREVO_API_KEY;

            let apiInstance = new SibApiV3Sdk.TransactionalEmailsApi();
            let sendSmtpEmail = new SibApiV3Sdk.SendSmtpEmail();

            //Build Email
            sendSmtpEmail.subject = mailBody.subject;
            sendSmtpEmail.htmlContent = mailBody.htmlContent;
            sendSmtpEmail.sender = {
                name: process.env.MAIL_API_FROM_NAME,
                email: process.env.MAIL_API_FROM_ADDRESS,
            };
            sendSmtpEmail.to = [
                { email: mailBody.sendToEmail, name: mailBody.sendToName },
            ];
            if (mailBody.sendCopyEmail) {
                sendSmtpEmail.cc = [
                    {
                        email: mailBody.sendCopyEmail,
                        name: mailBody.sendCopyName,
                    },
                ];
            }
            sendSmtpEmail.replyTo = {
                email: process.env.MAIL_API_NO_REPLY_EMAIL,
                name: process.env.MAIL_API_NO_REPLY_NAME,
            };

            sendSmtpEmail.templateId = mailBody.templateId;
            sendSmtpEmail.params = mailBody.params;

            await apiInstance.sendTransacEmail(sendSmtpEmail);

            return true;
        } catch (error) {
            handleError(error);
        }
    }
}
