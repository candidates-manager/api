//Imports
import handleError from '@/application/errors/handleError';
import { Injectable } from '@nestjs/common';

//Services
import BrevoService from './brevo.service';

//Entities
import { MailEntity } from 'talenthive-models/lib/Entities';

@Injectable()
export default class MailService {
    constructor(private readonly brevoService: BrevoService) {}
    async sendLostPasswordMail(data: {
        email: string;
        token: string;
        name: string;
    }): Promise<true | Error> {
        try {
            const { email, token, name } = data;
            const mailBody: MailEntity = {
                subject: 'Lost Password',
                htmlContent: 'Retrieve Password',
                sendToEmail: email,
                sendToName: name,
                templateId: parseInt(
                    process.env.MAIL_API_TEMPLATE_LOST_PASSWORD
                ),
                params: {
                    callToAction: `${process.env.FRONT_APP_LOST_PASSWORD_URL}?token=${token}`,
                },
            };

            await this.brevoService.send(mailBody);
            return true;
        } catch (error) {
            handleError(error);
        }
    }

    async sendActivateUserMail(data: {
        email: string;
        password: string;
        name: string;
    }): Promise<true | Error> {
        try {
            const { email, password, name } = data;
            const mailBody: MailEntity = {
                subject: 'Activate Account',
                htmlContent: 'Activate Account',
                sendToEmail: email,
                sendToName: name,
                templateId: parseInt(
                    process.env.MAIL_API_TEMPLATE_ACTIVATE_ACCOUNT
                ),
                params: {
                    callToAction: `${process.env.FRONT_APP_LOGIN_URL}`,
                    login: email,
                    password: password,
                },
            };

            await this.brevoService.send(mailBody);
            return true;
        } catch (error) {
            handleError(error);
        }
    }
}
