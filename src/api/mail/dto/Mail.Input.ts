import { Field, InputType } from '@nestjs/graphql';
import {
    IsEmail,
    IsNotEmpty,
    IsNumber,
    IsOptional,
    IsString,
} from 'class-validator';

// import { MailEntity } from 'talenthive-models/lib/Entities';

@InputType()
class ParamsMailInputDto {
    @Field()
    @IsOptional()
    @IsString()
    callToAction: string;
}

@InputType()
export class MailInputDto {
    @Field()
    @IsNotEmpty()
    @IsString()
    subject: string;
    @Field()
    @IsNotEmpty()
    @IsString()
    htmlContent: string;
    @Field()
    @IsNotEmpty()
    @IsEmail()
    sendToEmail: string;
    @Field()
    @IsNotEmpty()
    @IsString()
    sendToName: string;
    @Field()
    @IsNotEmpty()
    @IsNumber()
    templateId?: number;
    @Field()
    @IsEmail()
    @IsOptional()
    sendCopyEmail?: string;
    @Field()
    @IsString()
    @IsOptional()
    sendCopyName?: string;
    @Field(() => ParamsMailInputDto)
    @IsOptional()
    params?: ParamsMailInputDto;
}
