import { Module, Global } from '@nestjs/common';

//Controllers
import { MailController } from './mail.controller';

//Components
import BrevoService from './brevo.service';
import MailService from './mail.service';

@Module({
    controllers: [MailController],
    providers: [BrevoService, MailService],
    exports: [BrevoService, MailService],
})
export default class HelpersModule {}
