import { Controller, Post, Body } from '@nestjs/common';

//Service
import MailerSendService from './brevo.service';

//Pipes
import { ValidateParamId, ValidateBody } from '@/application/pipes';

//DTO
import { MailInputDto } from './dto/Mail.Input';

//Handle Error
import handleError from '@/application/errors/handleError';

@Controller('mail')
// @UseGuards(GqlAuthGuard)
export class MailController {
    constructor(private readonly mailerSendService: MailerSendService) {}

    @Post('send')
    async send(
        @Body(new ValidateBody()) body: MailInputDto
    ): Promise<void | Error> {
        try {
            await this.mailerSendService.send(body);
        } catch (error) {
            handleError(error, 'Cannot Send Email');
        }
    }
}
