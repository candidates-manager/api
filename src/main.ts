import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { ValidationPipe } from '@nestjs/common';
async function bootstrap() {
    console.log('Launching my App...');
    const app = await NestFactory.create(AppModule, { cors: true });
    app.enableCors({
        origin:
            process.env.CORS !== 'undefined' ?? process.env.CORS.split(', '),
        optionsSuccessStatus: 200,
    });
    app.useGlobalPipes(new ValidationPipe());
    await app.listen(process.env.PORT || 5000);
}
bootstrap();
