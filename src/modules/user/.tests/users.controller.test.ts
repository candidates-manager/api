/*
import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import mongoose from 'mongoose';

//Module Tested
import UserController from '../user.controller';
import UserService from '../user.service';
import UserProviders from '../user.provider';

//Dependencies
import DatabaseModule from '@Infrastructure/database/database.module';
import HelpersModule from '@Application/helpers/helpers.module';

//App Test
import AppTest from '../../../app.test.module';

describe('UserController', () => {
  let app: INestApplication;
  let controller: UserController;
  let service: UserService;
  let data;
  const public_id = 'jerome-charriaud';
  const userTest = {
    email: 'contact@jerome-charriaud.com',
    password: 'azerty',
    firstname: 'Jerome',
    lastname: 'Charriaud',
    role: [],
    updatedAt: new Date(),
    createdAt: new Date(),
  };

  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [AppTest, DatabaseModule, HelpersModule],
      providers: [UserController, UserService, ...UserProviders],
    }).compile();
    app = module.createNestApplication();
    await app.init();
    controller = module.get<UserController>(UserController);
    service = module.get<UserService>(UserService);

    data = await controller.register({ body: userTest });
  });

  afterAll(async () => {
    mongoose.connection.close();
    await app.close();
  });

  it('Components should be defined', () => {
    expect(app).toBeDefined();
    expect(controller).toBeDefined();
    expect(service).toBeDefined();
  });

  test('A User can register', async () => {
    expect(typeof data).toBe('boolean');
    expect(data).toBe(true);
  });

  test('A User can be deleted', async () => {
    const deletedUser = await controller.delete({ body: userTest });
    expect(typeof deletedUser).toBe('boolean');
    expect(deletedUser).toBe(true);
  });
});
*/
