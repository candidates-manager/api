/*
import * as request from 'supertest';
import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import mongoose from 'mongoose';

//GraphQl
const gql = '/graphql';

//Module Tested
import UserResolver from '../user.resolver';
import UserService from '../user.service';
import UserProviders from '../user.provider';

//App Test
import AppTest from '../../../app.test.module';

//Dependencies
import DatabaseModule from '@Infrastructure/database/database.module';
import HelpersModule from '@Application/helpers/helpers.module';

describe('UserModule', () => {
  let app: INestApplication;
  let resolver: UserResolver;
  let service: UserService;
  let data;
  const public_id = 'jerome-charriaud';

  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [AppTest, DatabaseModule, HelpersModule],
      providers: [UserResolver, UserService, ...UserProviders],
    }).compile();
    app = module.createNestApplication();
    await app.init();
    resolver = module.get<UserResolver>(UserResolver);
    service = module.get<UserService>(UserService);

    const result = await request(app.getHttpServer())
      .post(gql)
      .set('Authorization', `Bearer ${process.env.TOKEN_TEST}`)
      .send({
        query: `{user(public_id: "${public_id}") {public_id}}`,
      });
    if (result.body?.data) {
      data = result.body.data.user;
    }
  });
  afterAll(async () => {
    mongoose.connection.close();
    await app.close();
  });

  beforeEach(async () => {});

  it('Components should be defined', () => {
    expect(app).toBeDefined();
    expect(resolver).toBeDefined();
    expect(service).toBeDefined();
  });

  describe('User GraphQl Request', () => {
    test('Should Return a Non Empty Value', () => {
      expect(data).not.toBeNull();
    });
    test('Should Return an Object', () => {
      expect(data).toBeInstanceOf(Object);
    });
    test('Should Return Data Matching the Database', () => {
      expect(data[0]).toMatchObject({
        public_id: 'jerome-charriaud',
      });
    });
  });
});
*/
