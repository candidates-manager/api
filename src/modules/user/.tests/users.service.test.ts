import mongoose from 'mongoose';
import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';

//App Test
import AppTest from '../../../app.test.module';

//Media
import UserService from '../user.service';
import UserProvider from '../user.provider';

//Dependencies
import DatabaseModule from '@Infrastructure/database/database.module';
import HelpersModule from '@Application/helpers/helpers.module';

describe('UserService', () => {
  let app: INestApplication;
  let service: UserService;

  const userTest = {
    email: 'contact@jerome-charriaud.com',
    password: 'azerty',
    firstname: 'Jerome',
    lastname: 'Charriaud',
    role: [],
    updatedAt: new Date(),
    createdAt: new Date(),
  };

  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [AppTest, DatabaseModule, HelpersModule],
      providers: [UserService, ...UserProvider],
    }).compile();
    app = module.createNestApplication();
    service = app.get<UserService>(UserService);
    await app.init();
  });

  afterAll(async () => {
    mongoose.connection.close();
    await app.close();
  });

  it('Components should be defined', () => {
    expect(app).toBeDefined();
    expect(service).toBeDefined();
  });

  test('Service : Add User', async () => {
    const userAlreadyExists = await service.findUser(
      { 'email.address': userTest.email },
      {}
    );
    if (!userAlreadyExists) {
      const result = await service.addUser(userTest);
      expect(result).toBe(true);
    }
  });

  test('Service : Find User', async () => {
    const result = await service.findUser(
      { 'email.address': userTest.email },
      {}
    );
    expect(typeof result).toBe('object');
    expect(result).toHaveProperty('_id');
  });

  test('Service : Delete User', async () => {
    const result = await service.deleteUser({
      'email.address': userTest.email,
    });
    expect(result).toBe(true);
  });
});
