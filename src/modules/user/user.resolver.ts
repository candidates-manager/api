//Graphql
import { Resolver, Query, Args, ResolveField, Parent } from '@nestjs/graphql';

//Module
import { User } from './user.schema';

//Services
import UserService from './user.service';
import { RoleServices } from '@Modules/userRole/role.service';

//DTO
import { UserResponse } from './dto/user.response';
import { UserArgs } from './dto/user.args';
import { RoleResponse } from '@Modules/userRole/dto/role.response';
import { RoleArgs } from '@Modules/userRole/dto/role.args';

//Decorators
import {
    Roles,
    Projections,
    CompanyRestriction,
} from '@/application/decorators';

//Pipes
import { ValidateArgs } from '@Pipes/validate.args.pipe';

//Functions
import handleError from '@/application/errors/handleError';

@Resolver(() => UserResponse)
export default class UserResolver {
    constructor(
        private readonly UserService: UserService,
        private readonly RoleServices: RoleServices
    ) {}

    //Get Users
    @Query(() => [UserResponse])
    @Roles('admin', 'recruiter')
    @CompanyRestriction(true)
    async users(
        @Args(new ValidateArgs()) args: UserArgs,
        @Projections() projections
    ) {
        try {
            const users = <UserResponse[]>(
                await this.UserService.getUsers(args, projections)
            );
            return users;
        } catch (error) {
            handleError(error, 'Cannot Get Users');
        }
    }

    //Resolver - Applications
    // @ResolveField(() => [UserResponse])
    // async role_id(
    //     @Parent() user: UserResponse,
    //     @Args(new ValidateArgs()) args: RoleArgs,
    //     @Projections() projections: any
    // ): Promise<RoleResponse[] | Error> {
    //     try {
    //         const params = {
    //             ...args,
    //             _id: user.role_id,
    //         };
    //         return <RoleResponse[]>(
    //             await this.RoleServices.getRoles(params, projections)
    //         );
    //     } catch (error) {
    //         handleError(error, 'Cannot Get Roles');
    //     }
    // }
}
