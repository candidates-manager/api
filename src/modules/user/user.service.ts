import { Injectable } from '@nestjs/common';
import * as bcrypt from 'bcrypt';

//Mongoose
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';

//Schema
import { User, UserDocument } from './user.schema';

//DTO
import { UserInput } from './dto/user.input';
import { UserResponse } from './dto/user.response';
import { UserUpdate } from './dto/user.update';

//Services
import MailService from '@/api/mail/mail.service';

//Functions
import { generateRandomString } from '@Functions/generateRandomStrings';

//Error
import handleError from '@/application/errors/handleError';

@Injectable()
export default class UserServices {
    constructor(
        @InjectModel(User.name)
        private UserModel: Model<User>,
        private MailServices: MailService
    ) {}

    //Get Users
    async getUsers(
        params,
        projections,
        options = {}
    ): Promise<UserResponse[] | undefined> {
        try {
            const query = this.UserModel.find(params, projections);
            query.populate('role_id');
            query.populate('company_id');
            const response = <UserResponse[]>await query.lean();

            return response;
        } catch (error) {
            handleError(error, 'Cannot Find Users');
        }
    }

    //Find User
    async findUser(
        params,
        projections,
        options = {}
    ): Promise<UserResponse | undefined> {
        try {
            const query = this.UserModel.findOne(params, projections).select(
                '+password'
            );
            query.populate('role_id');
            query.populate('company_id');
            const response = <UserResponse>await query.lean();
            return response;
        } catch (error) {
            handleError(error, 'Cannot Find User');
        }
    }

    //Add User
    async addUser(data: UserInput): Promise<UserDocument> {
        try {
            //Fullname
            const fullname = data.firstname + ' ' + data.lastname;

            //Generate Password if does not exist
            if (!data.password) {
                data.password = generateRandomString(24);
            }

            //Generate Password
            const newPassword = await new Promise((resolve, reject) => {
                bcrypt.hash(data.password, 10, (err, hash) => {
                    if (err) {
                        reject('Error while hasing the password');
                    }
                    resolve(hash);
                });
            });

            //Gather the credentials
            const credentials = {
                email: {
                    address: data.email,
                    verified: true,
                },
                password: newPassword,
                firstname: data.firstname,
                lastname: data.lastname,
                company_id: data.company_id,
                role_id: data.role_id,
            };

            //Create the user
            const newUser = await this.UserModel.create(credentials);

            //Send an activation email
            await this.MailServices.sendActivateUserMail({
                email: data.email,
                password: data.password,
                name: fullname,
            });

            return newUser;
        } catch (error) {
            handleError(error, 'Cannot Add User');
        }
    }

    //Delete User
    async deleteUser(_id: string): Promise<Boolean | undefined> {
        try {
            const deletion = await this.UserModel.deleteOne({ _id });
            return true;
        } catch (error) {
            handleError(error, 'Cannot Delete User');
        }
    }

    //Change User Password
    async updateUserPassword(
        _id: string,
        password: string
    ): Promise<Boolean | Error> {
        try {
            //Check if password is different
            const user = await this.UserModel.findById(_id, { password: 1 });
            const currentPassword = user.password;

            const checkPasswordIsSame = await new Promise((resolve, reject) => {
                bcrypt.compare(password, currentPassword, (err, result) => {
                    if (err) {
                        reject();
                    } else {
                        resolve(result);
                    }
                });
            });
            if (checkPasswordIsSame) {
                throw new Error('Your password must not be the same');
            }
            //Generate Password
            const newPassword = await new Promise((resolve, reject) => {
                bcrypt.hash(password, 10, (err, hash) => {
                    if (err) {
                        reject('Error while hasing the password');
                    }
                    resolve(hash);
                });
            });

            await this.UserModel.findByIdAndUpdate(
                { _id },
                { password: newPassword }
            );
            return true;
        } catch (error) {
            handleError(error, error.message || 'Cannot Update User Password');
        }
    }

    //Update User
    async updateUser(
        _id: string,
        data: UserUpdate
    ): Promise<UserResponse | Error> {
        try {
            const updatedUser = <UserResponse>(
                await this.UserModel.findByIdAndUpdate({ _id }, data, {
                    new: true,
                })
            );
            return updatedUser;
        } catch (error: unknown) {
            handleError(error, 'Cannot Update User');
        }
    }

    //Count Users
    async count(params): Promise<number | Error> {
        try {
            return this.UserModel.count(params);
        } catch (error) {
            handleError(error, 'Cannot Count Users');
        }
    }
}
