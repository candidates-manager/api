import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { HydratedDocument, Types, ObjectId } from 'mongoose';

//Entities
import { UserEntity } from 'talenthive-models/lib/Entities';

//Schemas
import { Company } from '../company/company.schema';
import { Candidate } from '../candidate/candidate.schema';
import { Role } from '@Modules/userRole/role.schema';
import { UserStatusEnum } from 'talenthive-models/lib/Enums';

@Schema()
export class User implements UserEntity {
    _id: ObjectId;
    @Prop({
        required: [true, 'Email is required !'],
        type: String,
    })
    email: string;
    @Prop({
        required: [true, 'A password is required !'],
        validate: {
            validator: (v) => {
                return v.length >= 8 ?? true;
            },
            message: () =>
                'The name of the user must be at least 8 characters length',
        },
        select: false,
    })
    password: string;
    @Prop({
        required: [true, 'A name is required !'],
        validate: {
            validator: (v) => {
                return v.length >= 2 ?? true;
            },
            message: () =>
                'The name of the user must be at least 2 characters length',
        },
    })
    firstname: string;
    @Prop({
        required: [true, 'A name is required !'],
        validate: {
            validator: (v) => {
                return v.length >= 2 ?? true;
            },
            message: () =>
                'The name of the user must be at least 2 characters length',
        },
    })
    lastname: string;
    @Prop({
        required: false,
    })
    avatar: string;
    @Prop({
        required: false,
    })
    function: string;
    @Prop({
        type: {
            _id: false,
            linkedin: { type: String, unique: false },
        },
    })
    urls: { linkedin: string };
    @Prop({ type: [Types.ObjectId], ref: 'Company' })
    company_id: Company[];
    @Prop({ type: Types.ObjectId, ref: 'Candidate' })
    candidate_id: Candidate;
    @Prop({ type: [Types.ObjectId], ref: 'Role' })
    role_id: Role[];
    @Prop({ type: String, default: 'onboarding' })
    status: UserStatusEnum;
    @Prop({ default: new Date() })
    updatedAt?: Date;
    @Prop({ default: new Date() })
    createdAt?: Date;
}

export type UserDocument = HydratedDocument<User>;
export const UserSchema = SchemaFactory.createForClass(User);
