import {
    Controller,
    Body,
    Post,
    Get,
    Req,
    Patch,
    Param,
    Query,
} from '@nestjs/common';

//Services
import UserServices from '@Modules/user/user.service';

//DTO
import { UserArgs } from './dto/user.args';
import { UserInput } from './dto/user.input';
import { UserResponse } from './dto/user.response';

//Guards
import { CompanyRestriction, Roles, OnlyMe } from '@/application/decorators';

//Pipes
import {
    ValidateBody,
    ValidateParamId,
    ValidateUserBody,
} from '@/application/pipes';

//Handle Error
import handleError from '@/application/errors/handleError';

//Schema
import { UserDocument } from './user.schema';
import { UserUpdateModel } from 'talenthive-models/lib/Entities';

@Controller('users')
export default class UserController {
    constructor(private readonly UserServices: UserServices) {}

    //Add User
    @Post('/register')
    @Roles('admin', 'recruiter')
    @CompanyRestriction(true)
    async register(
        @Body(new ValidateBody(), new ValidateUserBody()) body: UserInput
    ): Promise<UserDocument | Error> {
        try {
            return <UserDocument>await this.UserServices.addUser(body);
        } catch (error) {
            handleError(error, 'Cannot create User');
        }
    }

    //Get Users
    @Get()
    @Roles('admin', 'recruiter')
    async getUsers(@Query() query: UserArgs): Promise<UserResponse[] | Error> {
        try {
            return this.UserServices.getUsers(query, {});
        } catch (error) {
            handleError(error, 'Cannot Get Users');
        }
    }

    //Get Current User
    @Get('/getCurrentUser')
    async getCurrentUser(@Req() req): Promise<UserResponse | Error> {
        try {
            const user_id = <string>req.user.user_id;
            const currentUser = <UserResponse>(
                await this.UserServices.findUser({ _id: user_id }, {})
            );
            return currentUser;
        } catch (error) {
            handleError(error, 'Cannot Get User');
        }
    }

    //Update User
    @Patch('/u/:user_id')
    @OnlyMe(true)
    async update(
        @Body(new ValidateBody(), new ValidateUserBody())
        body: UserUpdateModel,
        @Param('user_id', ValidateParamId)
        user_id: string
    ): Promise<UserResponse | Error> {
        try {
            const updatedUser = <UserResponse>(
                await this.UserServices.updateUser(user_id, body)
            );
            return updatedUser;
        } catch (error) {
            handleError(error, 'Cannot Update User');
        }
    }

    //Update User Password
    @Patch('/u/password/:user_id')
    @OnlyMe(true)
    async updatePassword(
        @Body('password')
        password: string,
        @Param('user_id', ValidateParamId)
        user_id: string
    ): Promise<Boolean | Error> {
        try {
            return <Boolean>(
                await this.UserServices.updateUserPassword(user_id, password)
            );
        } catch (error) {
            handleError(error, 'Cannot Update User Password');
        }
    }
}
