import { Field, ArgsType, InputType } from '@nestjs/graphql';
import { IsEmail, IsOptional } from 'class-validator';
import { UserArgsModel } from 'talenthive-models/lib/Entities';

@ArgsType()
export class UserArgs implements UserArgsModel {
    @Field(() => String, { nullable: true })
    @IsOptional()
    _id?: string;
    @Field(() => String, { nullable: true })
    @IsOptional()
    email?: string;
    @Field(() => String, { nullable: true })
    @IsOptional()
    role_public_id?: string;
    @Field(() => [String], { nullable: true })
    @IsOptional()
    company_id?: string[];
}
