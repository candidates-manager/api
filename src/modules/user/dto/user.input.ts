import { Field, InputType } from '@nestjs/graphql';

import {
    IsEmail,
    IsNotEmpty,
    MinLength,
    IsString,
    IsOptional,
    IsArray,
    IsStrongPassword,
} from 'class-validator';
import { UserInputModel } from 'talenthive-models/lib/Entities';
import { UserStatusEnum } from 'talenthive-models/lib/Enums';

@InputType()
export class UserInput implements UserInputModel {
    @Field()
    @IsEmail()
    @IsNotEmpty()
    email: string;
    @Field()
    @IsOptional()
    @IsNotEmpty()
    @MinLength(8)
    @IsStrongPassword()
    password: string;
    @Field()
    @IsString()
    @IsOptional()
    avatar: string;
    @Field()
    @IsString()
    firstname: string;
    @Field()
    @IsString()
    lastname: string;
    @Field()
    @IsString()
    @IsOptional()
    function: string;
    @Field()
    @IsString()
    @IsOptional()
    linkedin: string;
    @Field()
    @IsString()
    @IsOptional()
    status?: UserStatusEnum;
    @Field(() => [String])
    @IsArray()
    company_id: string[];
    @Field(() => [String])
    @IsArray()
    role_id: string[];
}
