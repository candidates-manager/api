import { Field, ObjectType } from '@nestjs/graphql';
import { IsDate, IsEmail, IsString } from 'class-validator';

//Entities
import { UserResponseModel } from 'talenthive-models/lib/Entities';

//Responses
import CandidateResponse from '@/modules/candidate/dto/candidate.response.dto';
import { RoleResponse } from '@Modules/userRole/dto/role.response';
import { CompanyResponse } from '@/modules/company/dto/company.response';
import { UserStatusEnum } from 'talenthive-models/lib/Enums';

@ObjectType()
class UserUrlsResponse {
    @Field()
    linkedin: string;
}
@ObjectType()
export class UserResponse implements UserResponseModel {
    @Field()
    @IsString()
    _id: string;
    @Field(() => String)
    @IsEmail()
    email: string;
    @Field()
    @IsString()
    password: string;
    @Field()
    @IsString()
    firstname: string;
    @Field()
    @IsString()
    lastname: string;
    @Field({ nullable: true })
    @IsString()
    function: string;
    @Field()
    @IsString()
    avatar: string;
    @Field(() => [RoleResponse], { nullable: true })
    role_id: RoleResponse[];
    @Field(() => [CompanyResponse])
    company_id: CompanyResponse[];
    @Field(() => CandidateResponse)
    candidate_id: CandidateResponse;
    @Field()
    status: UserStatusEnum;
    @Field(() => UserUrlsResponse)
    urls: UserUrlsResponse;
    @Field()
    @IsDate()
    updatedAt: Date;
    @Field()
    @IsDate()
    createdAt: Date;
}
