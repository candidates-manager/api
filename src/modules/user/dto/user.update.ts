import { Field, InputType } from '@nestjs/graphql';

import {
    IsEmail,
    IsStrongPassword,
    IsString,
    IsOptional,
} from 'class-validator';

import { UserUpdateModel } from 'talenthive-models/lib/Entities';
import { UserStatusEnum } from 'talenthive-models/lib/Enums';

@InputType()
export class UserUpdate implements UserUpdateModel {
    @Field()
    @IsEmail()
    email?: string;
    @Field()
    @IsStrongPassword()
    password?: string;
    @Field()
    @IsString()
    firstname?: string;
    @Field()
    @IsString()
    lastname?: string;
    @Field()
    @IsString()
    avatar?: string;
    @Field()
    @IsString()
    @IsOptional()
    linkedin?: string;
    @Field(() => [String])
    @IsString()
    company?: string[];
    @Field(() => [String])
    @IsString()
    role?: string[];
    @Field()
    @IsString()
    status?: UserStatusEnum;
}
