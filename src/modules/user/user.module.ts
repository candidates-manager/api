import { Module } from '@nestjs/common';

//Controller
import UserController from './user.controller';

//Resolver
import UserResolver from './user.resolver';

//Schema
import { MongooseModule } from '@nestjs/mongoose';
import { User, UserSchema } from './user.schema';

//Service
import UserService from './user.service';

//Modules
import MailModule from '@Api/mail/mail.module';
import RolesModule from '@Modules/userRole/role.module';

@Module({
    imports: [
        MongooseModule.forFeature([{ name: User.name, schema: UserSchema }]),
        RolesModule,
        MailModule,
    ],
    exports: [UserService],
    controllers: [UserController],
    providers: [UserResolver, UserService],
})
export default class UserModule {}
