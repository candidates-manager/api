import { Controller, Body, Post, Get, UseGuards, Query } from '@nestjs/common';
import { AuthService } from '@Modules/auth/auth.service';

//Functions
import handleError from '@Application/errors/handleError';

//Restrictions
import { AuthRestriction } from '@/application/decorators';

//DTO
import {
    LostPasswordDto,
    LoginDetailsdDto,
    ChangePasswordDto,
} from './dto/Auth.Input.dto';

//Models
import { AuthResponseModel } from 'talenthive-models/lib/Entities';

@Controller('auth')
export class AuthController {
    constructor(private readonly authService: AuthService) {}

    //Login
    @Post('/login')
    @AuthRestriction(false)
    async login(
        @Body() loginDetails: LoginDetailsdDto
    ): Promise<AuthResponseModel | Error> {
        try {
            return this.authService.login(loginDetails);
        } catch (error) {
            handleError(error, 'Cannot Login User');
        }
    }

    //Lost Password
    @Post('/lost-password')
    @AuthRestriction(false)
    async lostPassword(
        @Body() data: LostPasswordDto
    ): Promise<boolean | Error> {
        try {
            await this.authService.lostPassword(data);
            return true;
        } catch (error) {
            handleError(error, 'Cannot Generate Lost Password Token');
        }
    }

    //Change Password
    @Post('/change-password')
    @AuthRestriction(false)
    async changePassword(
        @Body() data: ChangePasswordDto,
        @Query() params
    ): Promise<boolean | Error> {
        try {
            return this.authService.changePassword(params.token, data);
        } catch (error) {
            handleError(error, 'Cannot Change Password');
        }
    }

    //Check Token
    @Get('/checkToken')
    async checkToken() {
        return true;
    }
}
