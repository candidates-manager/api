import { Module, forwardRef } from '@nestjs/common';
import { PassportModule } from '@nestjs/passport';
import { JwtModule } from '@nestjs/jwt';
import { ConfigService } from '@nestjs/config';

//Auth
import { AuthService } from './auth.service';
import { AuthController } from './auth.controller';
import JwtStrategy from './strategies/auth.strategy.jwt';

//Guards
import { GqlThrottlerGuard } from '../../application/guards/gqlThrottler.guard';

//Modules
import UsersModule from '@Modules/user/user.module';
import MailModule from '@Api/mail/mail.module';

@Module({
    imports: [
        forwardRef(() => UsersModule),
        PassportModule,
        MailModule,
        JwtModule.registerAsync({
            useFactory: async (configService: ConfigService) => ({
                secret: process.env.JWT_SECRET,
                signOptions: { expiresIn: '12h' },
            }),
            inject: [ConfigService],
        }),
    ],
    controllers: [AuthController],
    providers: [AuthService, ConfigService, JwtStrategy, GqlThrottlerGuard],
    exports: [AuthService],
})
export default class AuthModule {}
