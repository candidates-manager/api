import {
    IsEmail,
    IsString,
    IsNotEmpty,
    IsStrongPassword,
    MinLength,
} from 'class-validator';
import { InputType } from '@nestjs/graphql';

//Models
import { AuthInputModel } from 'talenthive-models/lib/Entities';
@InputType()
export class LostPasswordDto {
    @IsNotEmpty()
    @IsEmail()
    email: string;
}
@InputType()
export class ChangePasswordDto {
    @IsString()
    @IsNotEmpty()
    @MinLength(8)
    password: string;
}
@InputType()
export class LoginDetailsdDto implements AuthInputModel {
    @IsNotEmpty()
    @IsEmail()
    email: string;
    @IsNotEmpty()
    password: string;
}
