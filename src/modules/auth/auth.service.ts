import { Injectable, Inject, HttpStatus } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { sha256 } from 'js-sha256';
import * as bcrypt from 'bcrypt';

//Models
import {
    AuthPayloadEntity,
    AuthResponseModel,
} from 'talenthive-models/lib/Entities';

//DTO
import {
    LoginDetailsdDto,
    LostPasswordDto,
    ChangePasswordDto,
} from './dto/Auth.Input.dto';

//Services
import UsersService from '@Modules/user/user.service';
import MailService from '@Api/mail/mail.service';

//Errors
import { CustomHttpException } from '@/application/errors/customHttpException';

@Injectable()
export class AuthService {
    constructor(
        private readonly UsersServices: UsersService,
        private readonly jwtService: JwtService,
        private readonly mailService: MailService
    ) {}

    //User Validation
    async validateUser(data: LoginDetailsdDto): Promise<any> {
        try {
            const user = await this.UsersServices.findUser(
                {
                    'email.address': data.email,
                },
                {}
            );
            if (!user) return null;
            const passwordValid = bcrypt.compareSync(
                sha256(data.password),
                user.password
            );
            if (user && passwordValid) {
                const { ...result } = user;
                return result;
            }
            return null;
        } catch (e) {
            throw new Error(e);
        }
    }

    //Login
    async login(
        loginDetails: LoginDetailsdDto
    ): Promise<AuthResponseModel | Error> {
        try {
            const { email, password } = loginDetails;

            //1 - Let's check if the user exists
            const credentials = await this.UsersServices.findUser(
                {
                    email,
                },
                {}
            );

            if (!credentials) {
                throw new Error();
            }

            //3 - Let's check if the user password is legit
            const checkPassword = await new Promise((resolve, reject) => {
                bcrypt.compare(
                    password,
                    credentials.password,
                    (err, result) => {
                        if (err) {
                            reject();
                        } else {
                            resolve(result);
                        }
                    }
                );
            });

            if (!checkPassword) {
                throw new Error();
            }

            //4 - Build the Payload
            const payload: AuthPayloadEntity = {
                user_id: credentials._id.toString(),
            };

            //Company
            if (credentials.company_id && credentials.company_id.length > 0) {
                payload.companies = credentials.company_id.map((v) =>
                    v._id.toString()
                );
            }

            //Role
            if (credentials.role_id && credentials.role_id.length > 0) {
                payload.roles = credentials.role_id.map((v) => v.public_id);
            }

            //Candidate
            if (credentials.candidate_id) {
                payload.candidate = credentials.candidate_id.toString();
            }

            return {
                token: this.jwtService.sign(payload, {
                    expiresIn: '12h',
                }),
                credentials: credentials,
            };
        } catch (e) {
            throw new CustomHttpException({
                message: 'Your Email or Password is incorrect.',
                code: 'auth/login-password-incorrect',
                statusCode: HttpStatus.FORBIDDEN,
            });
        }
    }

    //Lost Password - Generate and Send Token via Email
    async lostPassword(data: LostPasswordDto): Promise<boolean | Error> {
        try {
            //Find User
            const user = await this.UsersServices.findUser(
                {
                    email: data.email,
                },
                {}
            );
            if (!user) return true;

            const payload = { email: data.email };
            const token = await this.jwtService.sign(payload, {
                expiresIn: '1h',
            });

            //Send Email
            await this.mailService.sendLostPasswordMail({
                email: data.email,
                token,
                name: user.firstname + ' ' + user.lastname,
            });
            return true;
        } catch {
            throw new CustomHttpException({
                message: 'Cannot Generate Lost Token Password',
                code: 'auth/cannot-generate-lost-password-token',
                statusCode: HttpStatus.FORBIDDEN,
            });
        }
    }
    //Lost Password - Change Password
    async changePassword(
        token: string,
        data: ChangePasswordDto
    ): Promise<boolean | Error> {
        try {
            //Check Token
            const payload = await this.jwtService.verifyAsync(token);
            const { email } = payload;

            //Find User
            const user = await this.UsersServices.findUser(
                {
                    email,
                },
                { password: 1 }
            );

            //Check If Password is the same
            const checkPasswordIsSame = await new Promise((resolve, reject) => {
                bcrypt.compare(data.password, user.password, (err, result) => {
                    if (err) {
                        reject();
                    } else {
                        resolve(result);
                    }
                });
            });

            if (checkPasswordIsSame) {
                throw new Error('Your password must not be the same');
            }

            //Change Password
            const newPassword = <string>await new Promise((resolve, reject) => {
                bcrypt.hash(data.password, 10, (err, hash) => {
                    if (err) {
                        throw new Error('Error while hasing the password');
                    }
                    resolve(hash);
                });
            });

            if (!user) throw new Error('User Not Found');

            //Insert it into Database
            await this.UsersServices.updateUser(user._id, {
                password: newPassword,
            });

            return true;
        } catch (error) {
            throw new CustomHttpException({
                message: error.message || 'Cannot Change Password',
                code: 'auth/cannot-change-password',
                statusCode: HttpStatus.FORBIDDEN,
            });
        }
    }
}
