//NestJs
import { HttpStatus } from '@nestjs/common';

//Graphql
import { Resolver, ResolveField, Query, Args, Parent } from '@nestjs/graphql';

//Module
import { Application } from './application.schema';
import ApplicationService from './application.service';
import { ApplicationResponse } from './dto/application.response';
import ApplicationArgs from './dto/application.args';

//Decorators
import { Projections } from '@Application/decorators/projections.decorator';

//Restrictions
import {
    CompanyRestriction,
    CandidateRestriction,
} from '@/application/decorators';

//Guards
import { Roles } from '@/application/decorators/roles.decorator';

//Pipes
import { ValidateArgs } from '@Pipes/validate.args.pipe';

//Handle Error
import handleError from '@/application/errors/handleError';
import { CustomHttpException } from '@/application/errors/customHttpException';
import { ApplicationStepServices } from '../applicationStep/applicationStep.service';

@Resolver(() => ApplicationResponse)
export default class ApplicationResolver {
    constructor(
        private readonly ApplicationService: ApplicationService,
        private readonly ApplicationStepService: ApplicationStepServices
    ) {}

    @Query(() => [ApplicationResponse])
    @Roles('recruiter', 'candidate')
    @CompanyRestriction(true)
    @CandidateRestriction(true)
    async applications(
        @Args(new ValidateArgs()) args: ApplicationArgs,
        @Projections() projections
    ) {
        try {
            const applications = <ApplicationResponse[]>(
                await this.ApplicationService.getApplications(args, projections)
            );

            if (!applications || applications.length === 0) {
                throw new CustomHttpException({
                    message: 'Cannot Find Applications',
                    code: 'applications/not-found',
                    statusCode: HttpStatus.NOT_FOUND,
                });
            }
            return applications;
        } catch (error) {
            handleError(error, 'Cannot Find Applications');
        }
    }
}
