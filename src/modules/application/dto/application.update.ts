import { Field, InputType } from '@nestjs/graphql';

import {
    ApplicationFeedbackEntity,
    ApplicationProcessStepUpdateModel,
    UserEntity,
} from 'talenthive-models/lib/Entities';

//Entities
import { ApplicationUpdateModel } from 'talenthive-models/lib/Entities';

//Enums
import ApplicationStatusEnum from 'talenthive-models/lib/Enums/Application.Status.Enum';
import { ApplicationOrigin } from 'talenthive-models/lib/Enums';
import { IsNumber, IsOptional, IsUrl } from 'class-validator';

@InputType()
export class ApplicationFeedbackUpdate implements ApplicationFeedbackEntity {
    @Field(() => String)
    feedback_reasons: string;
    @Field(() => String)
    @IsOptional()
    feedback_strenghts?: string;
    @Field(() => String)
    @IsOptional()
    feedback_improvements?: string;
    @Field(() => Boolean)
    @IsOptional()
    request_glassdoor: boolean;
    @Field(() => String)
    createdBy: UserEntity | UserEntity['_id'];
    @Field(() => String)
    createdAt?: Date;
    @Field(() => String)
    updatedAt?: Date;
}

@InputType()
export class ApplicationUpdate implements ApplicationUpdateModel {
    @Field(() => ApplicationStatusEnum)
    status?: ApplicationStatusEnum;
    @Field(() => ApplicationOrigin)
    origin?: ApplicationOrigin;
    @Field(() => ApplicationFeedbackUpdate)
    feedback?: ApplicationFeedbackUpdate;
    @Field(() => Number)
    @IsNumber()
    @IsOptional()
    currentStep?: Number;
    @Field(() => Date)
    startAt?: Date;
}
