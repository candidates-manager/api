import ApplicationStepInput from '@/modules/applicationStep/dto/ApplicationStep.input.dto';
import { Field, InputType } from '@nestjs/graphql';
import { IsDate } from 'class-validator';

import { ApplicationInputModel } from 'talenthive-models/lib/Entities';

@InputType()
export default class ApplicationInput implements ApplicationInputModel {
    @Field(() => String)
    job_id: string;
    @Field(() => String)
    candidate_id: string;
    @Field()
    origin?: String;
    @Field(() => [ApplicationStepInput], { nullable: true })
    steps?: ApplicationStepInput[];
    @Field(() => Date)
    @IsDate()
    startAt?: Date;
}
