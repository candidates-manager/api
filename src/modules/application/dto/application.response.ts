import { Field, ObjectType } from '@nestjs/graphql';

//Entities
import { ApplicationResponseModel } from 'talenthive-models/lib/Entities';

//Responses
import JobResponse from '@/modules/job/dto/job.response';
import { ApplicationStepResponse } from '@Modules/applicationStep/dto/applicationStep.response';
import CandidateResponse from '@/modules/candidate/dto/candidate.response.dto';
//Enums
import ApplicationStateEnum from 'talenthive-models/lib/enums/Application.Status.Enum';
import { IsDate } from 'class-validator';

@ObjectType()
export class ApplicationResponse implements ApplicationResponseModel {
    @Field()
    _id: string;
    @Field(() => JobResponse, { nullable: true })
    job_id: JobResponse;
    @Field(() => CandidateResponse, { nullable: true })
    candidate_id: CandidateResponse;
    @Field()
    status: ApplicationStateEnum;
    @Field(() => [ApplicationStepResponse], { nullable: true })
    steps?: ApplicationStepResponse[];
    @Field()
    @IsDate()
    startAt?: Date;
    @Field()
    @IsDate()
    createdAt?: Date;
    @Field()
    @IsDate()
    updatedAt?: Date;
    @Field(() => String, { nullable: true })
    company: string;
}
