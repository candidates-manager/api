import { Field, ArgsType } from '@nestjs/graphql';
import { IsOptional } from 'class-validator';

//Entities
import { ApplicationArgsModel } from 'talenthive-models/lib/Entities';
import StatusEnum from 'talenthive-models/lib/enums/Application.Status.Enum';

@ArgsType()
export default class ApplicationArgs implements ApplicationArgsModel {
    @Field(() => [String], { nullable: true })
    @IsOptional()
    application_id?: string[];
    @Field(() => [String], { nullable: true })
    @IsOptional()
    candidate_id?: string[];
    @Field(() => [String], { nullable: true })
    @IsOptional()
    status?: [StatusEnum];
}
