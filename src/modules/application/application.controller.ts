import {
    Controller,
    Body,
    Get,
    Post,
    Param,
    Patch,
    Query,
} from '@nestjs/common';

//Services
import ApplicationServices from '@Modules/application/application.service';

//Handle Error
import handleError from '@/application/errors/handleError';

//Restrictions
import {
    CompanyRestriction,
    CandidateRestriction,
    AuthRestriction,
} from '@/application/decorators';

//DTO
import { ApplicationResponse } from './dto/application.response';
import { ApplicationUpdate } from './dto/application.update';

//Guards
import { Roles } from '@/application/decorators/roles.decorator';

//Pipes
import {
    ValidateBody,
    ValidateParamId,
    ValidationBodyApplication,
} from '@/application/pipes';

//Controller
@Controller('applications')
export default class ApplicationController {
    constructor(private readonly ApplicationServices: ApplicationServices) {}

    //Add Application
    @Post('/add')
    @Roles('candidate')
    @CandidateRestriction(true)
    async add(@Body(new ValidateBody()) body): Promise<void | Error> {
        try {
            this.ApplicationServices.addApplication(body);
        } catch (error) {
            handleError(error, 'Cannot Add Application');
        }
    }

    //Get Application
    @Get('/v/:application_id')
    @Roles('recruiter', 'candidate')
    @CompanyRestriction(true)
    @CandidateRestriction(true)
    async get(
        @Param('application_id', ValidateParamId)
        application_id: string
    ): Promise<ApplicationResponse | Error> {
        try {
            const application = <ApplicationResponse>(
                await this.ApplicationServices.getApplication({
                    _id: application_id,
                })
            );
            return application;
        } catch (error) {
            handleError(error, 'Cannot Get Application');
        }
    }

    //Get Candidate Application
    @Get('/myApplication/:application_id')
    @AuthRestriction(false)
    async getCandidateApplication(
        @Param('application_id', ValidateParamId)
        application_id: string,
        @Query('candidate_id', ValidateParamId)
        candidate_id: string
    ): Promise<ApplicationResponse | Error> {
        try {
            const application = <ApplicationResponse>(
                await this.ApplicationServices.getApplication(
                    { _id: application_id, candidate_id: candidate_id },
                    {}
                )
            );
            if (!application) throw Error();
            return application;
        } catch (error) {
            handleError(error, 'Cannot Get Candidate');
        }
    }

    //Update Application
    @Patch('/u/:application_id')
    @Roles('recruiter')
    @CompanyRestriction(true)
    async update(
        @Body(new ValidateBody(), new ValidationBodyApplication())
        body: ApplicationUpdate,
        @Param('application_id', ValidateParamId)
        application_id: string
    ): Promise<ApplicationResponse | Error> {
        try {
            const application = <ApplicationResponse>(
                await this.ApplicationServices.updateApplication(
                    application_id,
                    body
                )
            );
            return application;
        } catch (error) {
            handleError(error, 'Cannot Update Application');
        }
    }
}
