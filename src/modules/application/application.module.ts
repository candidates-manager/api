import { Module, forwardRef } from '@nestjs/common';

//Controller
import ApplicationController from './application.controller';

//Resolver
import ApplicationResolver from './application.resolver';

//Schema
import { MongooseModule } from '@nestjs/mongoose';
import { Application, ApplicationSchema } from './application.schema';

//Service
import ApplicationService from './application.service';

//Modules
import JobModule from '@Modules/job/job.module';
import ApplicationStepModule from '../applicationStep/applicationStep.module';

@Module({
    imports: [
        MongooseModule.forFeature([
            { name: Application.name, schema: ApplicationSchema },
        ]),
        forwardRef(() => JobModule),
        ApplicationStepModule,
    ],
    exports: [ApplicationService],
    controllers: [ApplicationController],
    providers: [ApplicationResolver, ApplicationService],
})
export default class ApplicationModule {}
