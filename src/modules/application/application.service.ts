import { Injectable } from '@nestjs/common';

//Mongoose
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';

//Schema
import { Application, ApplicationDocument } from './application.schema';

//DTO
import ApplicationCreateDto from './dto/application.input.dto';
import { ApplicationUpdate } from './dto/application.update';

//Response
import { ApplicationResponse } from './dto/application.response';
import { ApplicationStepResponse } from '../applicationStep/dto/applicationStep.response';

//Services
import { ApplicationStepServices } from '@Modules/applicationStep/applicationStep.service';

//Handle Error
import handleError from '@/application/errors/handleError';

@Injectable()
export default class ApplicationServices {
    constructor(
        @InjectModel(Application.name)
        private ApplicationModel: Model<Application>,
        private ApplicationStepServices: ApplicationStepServices
    ) {}

    //Find Applications
    async getApplications(
        params,
        projections = null,
        options = {}
    ): Promise<ApplicationResponse[] | Error> {
        try {
            const query = this.ApplicationModel.find(params, projections);
            query.populate('job_id');
            query.populate('candidate_id');

            const response = <ApplicationResponse[]>await query.lean();

            return response;
        } catch (error: unknown) {
            handleError(error, 'Cannot Find Applications');
        }
    }

    //Get Application
    async getApplication(
        params,
        projections = null,
        options = {}
    ): Promise<ApplicationResponse | Error> {
        try {
            const query = this.ApplicationModel.findOne(params, projections);
            query.populate('job_id');
            query.populate('candidate_id');
            const response = <ApplicationResponse>await query.lean();

            //Populate with steps
            const steps = <ApplicationStepResponse[]>(
                await this.ApplicationStepServices.getApplicationSteps({
                    application_id: response._id,
                })
            );
            response.steps = steps;
            return response;
        } catch (error: unknown) {
            handleError(error, 'Cannot Find Application');
        }
    }

    //Add Application
    async addApplication(
        data: ApplicationCreateDto
    ): Promise<ApplicationDocument | Error> {
        try {
            const newApplication = <ApplicationDocument>(
                await this.ApplicationModel.create(data)
            );
            return newApplication;
        } catch (error: unknown) {
            handleError(error, 'Cannot Add Application');
        }
    }

    //Update Application
    async updateApplication(
        _id: string,
        data: ApplicationUpdate
    ): Promise<ApplicationResponse | Error> {
        try {
            const updatedApplication = <ApplicationResponse>(
                await this.ApplicationModel.findByIdAndUpdate({ _id }, data, {
                    new: true,
                })
            );

            return updatedApplication;
        } catch (error: unknown) {
            handleError(error, 'Cannot Update Application');
        }
    }
}
