import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import * as mongoose from 'mongoose';
import { HydratedDocument, Types, ObjectId } from 'mongoose';

//Schemas
import { Job } from '../job/job.schema';
import { Candidate } from '../candidate/candidate.schema';
import { Application_Step } from '../applicationStep/applicationStep.schema';

//Entity
import {
    ApplicationEntity,
    ApplicationStepResponseModel,
    ApplicationFeedbackEntity,
} from 'talenthive-models/lib/Entities';

//Enums
import {
    ApplicationStatusEnum,
    ApplicationOrigin,
} from 'talenthive-models/lib/Enums';

@Schema({ autoIndex: true })
export class Application implements ApplicationEntity {
    _id: ObjectId;
    @Prop({
        type: mongoose.Schema.Types.ObjectId,
        ref: Job.name,
        required: [true, 'Job Id is required !'],
        unique: false,
    })
    job_id: Job;
    @Prop({
        type: mongoose.Schema.Types.ObjectId,
        required: [true, 'Candidate Id is required !'],
        ref: Candidate.name,
        unique: false,
    })
    candidate_id: Candidate;
    @Prop({
        unique: false,
        default: 'inProcess',
    })
    status: ApplicationStatusEnum;
    @Prop()
    origin: ApplicationOrigin;
    @Prop({ default: 1 })
    currentStep: Number;
    @Prop({
        type: [mongoose.Schema.Types.ObjectId],
        required: [true, 'Step Id is required !'],
        ref: Application_Step.name,
        unique: false,
    })
    steps: ApplicationStepResponseModel[];
    @Prop({
        required: false,
        type: {
            _id: false,
            feedback_reasons: { type: String, required: true },
            feedback_strenghts: { type: String, required: false },
            feedback_improvements: { type: String, required: false },
            request_glassdoor: {
                type: Boolean,
                default: false,
            },
            createdBy: { type: Types.ObjectId, immutable: true },
            createdAt: { type: Date, default: new Date(), immutable: true },
            updatedAt: { type: Date, default: new Date() },
        },
    })
    feedbacks: ApplicationFeedbackEntity;
    @Prop({
        required: true,
        default: new Date(),
    })
    startAt?: Date;
    @Prop({
        default: new Date(),
    })
    updatedAt?: Date;
    @Prop({
        default: new Date(),
    })
    createdAt?: Date;
}
export const ApplicationSchema = SchemaFactory.createForClass(Application);

//Application must be unique with the combination of one job_id and one candidate_id
ApplicationSchema.index({ job_id: 1, candidate_id: 1 }, { unique: true });
export type ApplicationDocument = HydratedDocument<Application>;
