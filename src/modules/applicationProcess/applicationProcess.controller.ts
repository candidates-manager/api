import {
    Controller,
    Body,
    Post,
    Param,
    Get,
    Patch,
    Delete,
    Req,
} from '@nestjs/common';

//Response
import { ApplicationProcessResponse } from './dto/applicationprocess.response';

//Services
import ApplicationProcessServices from './applicationProcess.service';

//DTO
import { ApplicationProcessInput } from './dto/applicationprocess.input.dto';

//Pipes
import {
    ValidateParamId,
    ValidateBody,
    ValidateBodyApplicationProcess,
} from '@/application/pipes';

//Restrictions
import { Roles, CompanyRestriction } from '@/application/decorators';

//Handle Error
import handleError from '@/application/errors/handleError';
import { ApplicationProcessDocument } from './applicationprocess.schema';
import { ApplicationProcessUpdate } from './dto/applicationProcess.update.dto';

@Controller('applications/processes')
export default class ApplicationProcessController {
    constructor(
        private readonly ApplicationProcessServices: ApplicationProcessServices
    ) {}

    //Add Application Process
    @Post('/add')
    @Roles('recruiter')
    @CompanyRestriction(true)
    async add(
        @Body(new ValidateBodyApplicationProcess())
        body: ApplicationProcessInput
    ): Promise<ApplicationProcessDocument | Error> {
        try {
            return <ApplicationProcessDocument>(
                await this.ApplicationProcessServices.addApplicationProcess(
                    body
                )
            );
        } catch (error) {
            handleError(error, 'Cannot Add Job Process');
        }
    }

    //Get Application Process
    @Get('/v/:_id')
    @Roles('recruiter')
    @CompanyRestriction(true)
    async get(
        @Param('_id', ValidateParamId)
        _id: string,
        @Req() req
    ): Promise<ApplicationProcessResponse | Error> {
        try {
            const applicationProcess = <ApplicationProcessResponse>(
                await this.ApplicationProcessServices.getApplicationProcess({
                    _id,
                })
            );
            return applicationProcess;
        } catch (error) {
            handleError(error, 'Cannot Get Job');
        }
    }

    //Update Application Process
    @Patch('/u/:_id')
    @Roles('recruiter')
    @CompanyRestriction(true)
    async update(
        @Body(new ValidateBody()) body: ApplicationProcessUpdate,
        @Param('_id', ValidateParamId)
        _id: string
    ): Promise<ApplicationProcessResponse | Error> {
        try {
            return await this.ApplicationProcessServices.updateApplicationProcess(
                _id,
                body
            );
        } catch (error) {
            handleError(error, 'Cannot Update Job Process');
        }
    }

    //Delete Application Process
    @Delete('/d/:_id')
    @Roles('recruiter')
    @CompanyRestriction(true)
    async delete(
        @Param('_id', ValidateParamId)
        _id: string
    ): Promise<boolean | Error> {
        try {
            return await this.ApplicationProcessServices.deleteApplicationProcess(
                _id
            );
        } catch (error) {
            handleError(error, 'Cannot Delete Job Process');
        }
    }
}
