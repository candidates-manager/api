//Graphql
import { Resolver, Query, Args } from '@nestjs/graphql';

//Module
import ApplicationProcessService from './applicationProcess.service';
import { ApplicationProcessResponse } from './dto/applicationProcess.response';
import { ApplicationProcessArgs } from './dto/applicationprocess.args';

//Decorators
import { Projections } from '@/application/decorators';

//Restrictions
import { Roles, CompanyRestriction } from '@/application/decorators';

//Pipes
import { ValidateArgs } from '@/application/pipes/validate.args.pipe';

//Handle Error
import handleError from '@/application/errors/handleError';

@Resolver(() => [ApplicationProcessResponse])
export default class ApplicationProcessResolver {
    //Dependencies
    constructor(
        private readonly ApplicationProcessService: ApplicationProcessService
    ) {}

    //Query - ApplicationProcesss
    @Query(() => [ApplicationProcessResponse])
    @Roles('recruiter')
    @CompanyRestriction(true)
    async application_processses(
        @Args(new ValidateArgs())
        args: ApplicationProcessArgs,
        @Projections() projections
    ) {
        try {
            const applicationprocesss = <[ApplicationProcessResponse]>(
                await this.ApplicationProcessService.getApplicationProcesses(
                    args,
                    projections
                )
            );
            return applicationprocesss;
        } catch (error) {
            handleError(error, 'Cannot Find Application Processs');
        }
    }
}
