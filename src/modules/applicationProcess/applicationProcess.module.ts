//NestJs
import { Module, forwardRef } from '@nestjs/common';

//Controller
import ApplicationProcessController from './applicationprocess.controller';

//Resolver
import ApplicationProcessResolver from './applicationprocess.resolver';

//Schema
import { MongooseModule } from '@nestjs/mongoose';
import {
    Application_Process,
    ApplicationProcessSchema,
} from './applicationprocess.schema';

//Service
import ApplicationProcessService from './applicationProcess.service';

//Modules
@Module({
    imports: [
        MongooseModule.forFeature([
            {
                name: Application_Process.name,
                schema: ApplicationProcessSchema,
            },
        ]),
    ],
    exports: [ApplicationProcessService],
    controllers: [ApplicationProcessController],
    providers: [ApplicationProcessResolver, ApplicationProcessService],
})
export default class ApplicationProcessModule {}
