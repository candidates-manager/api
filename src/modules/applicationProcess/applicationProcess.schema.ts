import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { HydratedDocument, Types, ObjectId } from 'mongoose';

//Schemas
import { Company } from '../company/company.schema';
import { User } from '../user/user.schema';

//Entity
import {
    ApplicationProcessEntity,
    ApplicationProcessStepEntity,
} from 'talenthive-models/lib/Entities';

@Schema()
export class Application_Process implements ApplicationProcessEntity {
    _id: ObjectId;
    @Prop({
        type: [Types.ObjectId],
        required: [true, 'A Company is required !'],
        unique: false,
        ref: 'Company',
    })
    company_id: [Company];
    @Prop({
        type: String,
        required: [true, 'A name is required !'],
        unique: false,
        validate: {
            validator: (v) => {
                return v.length >= 3 ?? true;
            },
            message: () =>
                'The name of the Job Process must be at least 3 characters length',
        },
    })
    name: string;
    @Prop({
        required: false,
        type: [
            {
                _id: false,
                step: { type: Number },
                name: { type: String },
                description: { type: String },
                duration: { type: Number },
                users: { type: Array, ref: 'User' },
            },
        ],
    })
    steps: ApplicationProcessStepEntity[];
    @Prop({
        default: new Date(),
        required: false,
    })
    updatedAt?: Date;
    @Prop({
        default: new Date(),
        required: false,
    })
    createdAt?: Date;
}
export const ApplicationProcessSchema =
    SchemaFactory.createForClass(Application_Process);
export type ApplicationProcessDocument = HydratedDocument<Application_Process>;
