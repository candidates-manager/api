import { Injectable } from '@nestjs/common';

//Mongoose
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';

//Schema
import {
    Application_Process,
    ApplicationProcessDocument,
} from './applicationprocess.schema';

//DTO
import { ApplicationProcessArgs } from './dto/applicationprocess.args';
import { ApplicationProcessInput } from './dto/applicationprocess.input.dto';
import { ApplicationProcessResponse } from './dto/applicationprocess.response';

//Handle Error
import handleError from '@/application/errors/handleError';
import { ApplicationProcessUpdate } from './dto/applicationProcess.update.dto';

@Injectable()
export default class ApplicationProcessServices {
    constructor(
        @InjectModel(Application_Process.name)
        private ApplicationProcessModel: Model<Application_Process>
    ) {}

    //Get Job Processses
    async getApplicationProcesses(
        params: ApplicationProcessArgs,
        projections = null,
        options = {}
    ): Promise<[ApplicationProcessResponse] | Error> {
        try {
            const query = this.ApplicationProcessModel.find(
                params,
                projections
            );
            query.populate('company_id');
            const response = <[ApplicationProcessResponse]>await query.lean();
            return response;
        } catch (error: unknown) {
            handleError(error, 'Cannot Find Job Processs');
        }
    }

    //Add ApplicationProcess
    async addApplicationProcess(
        params: ApplicationProcessInput
    ): Promise<ApplicationProcessDocument | Error> {
        try {
            const createdApplicationProcess = <ApplicationProcessDocument>(
                await this.ApplicationProcessModel.create(params)
            );
            return createdApplicationProcess;
        } catch (error: unknown) {
            handleError(error, 'Cannot Add Application Process');
        }
    }

    //Update ApplicationProcess
    async updateApplicationProcess(
        _id: string,
        data: ApplicationProcessUpdate
    ): Promise<ApplicationProcessResponse | Error> {
        try {
            const updatedApplicationProcess = <ApplicationProcessResponse>(
                await this.ApplicationProcessModel.findByIdAndUpdate(
                    { _id },
                    data,
                    {
                        new: true,
                    }
                )
            );

            return updatedApplicationProcess;
        } catch (error: unknown) {
            handleError(error, 'Cannot Add Job Process');
        }
    }

    //Get ApplicationProcess
    async getApplicationProcess(
        params,
        projections = null,
        options = {}
    ): Promise<ApplicationProcessResponse | Error> {
        try {
            const query = this.ApplicationProcessModel.findOne(
                params,
                projections
            );
            // query.populate('company_id');
            // query.populate('steps.users');
            const response = <ApplicationProcessResponse>await query.lean();

            if (!response) throw new Error();

            return response;
        } catch (error: unknown) {
            handleError(error, 'Cannot Find Job Process');
        }
    }

    //Delete ApplicationProcess
    async deleteApplicationProcess(_id: string): Promise<boolean | Error> {
        try {
            const response =
                await this.ApplicationProcessModel.findByIdAndRemove(_id);
            if (response) return true;
            else return false;
        } catch (error: unknown) {
            handleError(error, 'Cannot Delete Job Process');
        }
    }

    //Count ApplicationProcess
    async count(params): Promise<number | Error> {
        try {
            return this.ApplicationProcessModel.count(params);
        } catch (error) {
            handleError(error, 'Cannot Count Job Processs');
        }
    }
}
