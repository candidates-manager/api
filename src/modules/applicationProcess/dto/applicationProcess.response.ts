import { CompanyResponse } from '@/modules/company/dto/company.response';
import { UserResponse } from '@/modules/user/dto/user.response';
import { Field, ObjectType } from '@nestjs/graphql';
import { Document } from 'mongoose';

//Entity
import { ApplicationProcessResponseModel } from 'talenthive-models/lib/Entities';

@ObjectType('ApplicationProcessStepResponse')
class ApplicationProcessStepResponse {
    @Field()
    step: number;
    @Field()
    name: string;
    @Field()
    description: string;
    @Field()
    duration: number;
    @Field(() => [UserResponse], { nullable: true })
    users: UserResponse[];
}

@ObjectType()
export class ApplicationProcessResponse
    extends Document
    implements ApplicationProcessResponseModel
{
    @Field()
    _id: string;
    @Field(() => [CompanyResponse])
    company_id: CompanyResponse[];
    @Field()
    name: string;
    @Field(() => [ApplicationProcessStepResponse])
    steps: ApplicationProcessStepResponse[];
    @Field({ nullable: true })
    updatedAt: Date;
    @Field({ nullable: true })
    createdAt: Date;
}
