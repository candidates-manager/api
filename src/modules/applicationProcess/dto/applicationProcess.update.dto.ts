import { Field, InputType } from '@nestjs/graphql';
import { IsNumber, IsString } from 'class-validator';

//Entity
import {
    ApplicationProcessStepUpdateModel,
    ApplicationProcessUpdateModel,
} from 'talenthive-models/lib/Entities';

@InputType()
class ApplicationProcessStepInput implements ApplicationProcessStepUpdateModel {
    @Field()
    @IsNumber()
    step: Number;
    @Field()
    @IsString()
    name: String;
    @Field()
    @IsString()
    description?: String;
    @Field()
    @IsNumber()
    duration: Number;
}

@InputType()
export class ApplicationProcessUpdate implements ApplicationProcessUpdateModel {
    @Field()
    name?: string;
    @Field(() => [ApplicationProcessStepInput])
    steps?: ApplicationProcessStepInput[];
}
