import { Field, ArgsType } from '@nestjs/graphql';
import { IsArray, IsOptional } from 'class-validator';
import { ApplicationProcessArgsModel } from 'talenthive-models/lib/Entities';
//Entities

@ArgsType()
export class ApplicationProcessArgs implements ApplicationProcessArgsModel {
    @Field(() => [String], { nullable: true })
    @IsArray()
    company_id: string[];
    @Field(() => String, { nullable: true })
    @IsOptional()
    _id?: string;
}
