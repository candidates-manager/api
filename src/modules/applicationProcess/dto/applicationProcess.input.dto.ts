import { Field, InputType } from '@nestjs/graphql';
import { IsNumber, IsString, IsArray } from 'class-validator';
import {
    ApplicationProcessInputModel,
    ApplicationProcessStepInputModel,
} from 'talenthive-models/lib/Entities';

@InputType()
class ApplicationProcessStepInput implements ApplicationProcessStepInputModel {
    @Field()
    @IsNumber()
    step: Number;
    @Field()
    @IsString()
    name: String;
    @Field()
    @IsString()
    description?: String;
    @Field()
    @IsNumber()
    duration: Number;
}

@InputType()
export class ApplicationProcessInput implements ApplicationProcessInputModel {
    @Field()
    @IsString()
    name: string;
    @Field(() => [String])
    @IsArray()
    company_id: string[];
    @Field(() => [ApplicationProcessStepInput])
    steps: ApplicationProcessStepInput[];
}
