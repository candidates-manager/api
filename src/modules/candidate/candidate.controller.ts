import {
    Controller,
    Body,
    Post,
    Get,
    UseGuards,
    Param,
    Query,
} from '@nestjs/common';

//Service
import CandidateServices from '@Modules/candidate/candidate.service';

//Restrictions
import {
    Roles,
    CompanyRestriction,
    AuthRestriction,
} from '@/application/decorators';

//DTO
import { CandidateInput } from './dto/candidate.input.dto';

//Handle Error
import handleError from '@/application/errors/handleError';

//Pipes
import { ValidateParamId, ValidateBody } from '@/application/pipes';

//Entities
import { CandidateEntity } from 'talenthive-models/lib/Entities';
import CandidateResponse from './dto/candidate.response.dto';
import CandidateArgs from './dto/candidate.args.dto';

//Controller
@Controller('candidates')
export default class CandidateController {
    constructor(private readonly CandidateServices: CandidateServices) {}

    //Add Candidate
    @Post('/add')
    @Roles('recruiter')
    @CompanyRestriction(true)
    async register(
        @Body(new ValidateBody()) body: CandidateInput
    ): Promise<CandidateEntity | Error> {
        try {
            const newCandidate = <CandidateEntity>(
                await this.CandidateServices.addCandidate(body)
            );
            if (!newCandidate) throw Error();
            return newCandidate;
        } catch (error) {
            handleError(error, 'Cannot create Candidate');
        }
    }

    //Get  Candidates
    @Get()
    @Roles('admin', 'recruiter')
    async get(
        @Query() query: CandidateArgs
    ): Promise<CandidateResponse[] | Error> {
        try {
            const candidate = <CandidateResponse[]>(
                await this.CandidateServices.getCandidates(query, {})
            );
            if (!candidate) throw Error();
            return candidate;
        } catch (error) {
            handleError(error, 'Cannot Get Candidates');
        }
    }

    //Get Current Candidate
    @Get('/getCurrentCandidate/:access_token')
    @AuthRestriction(false)
    async getCurrentCandidate(
        @Param('access_token')
        access_token: string
    ): Promise<CandidateResponse | Error> {
        try {
            const candidate = <CandidateResponse>(
                await this.CandidateServices.getCandidate({ access_token }, {})
            );
            if (!candidate) throw Error();
            return candidate;
        } catch (error) {
            handleError(error, 'Cannot Get Candidate');
        }
    }
}
