import { Schema, SchemaFactory, Prop } from '@nestjs/mongoose';
import * as mongoose from 'mongoose';
import { HydratedDocument, Types, ObjectId } from 'mongoose';

//Entity
import {
    CandidateEntity,
    JobCategoryEntity,
} from 'talenthive-models/lib/Entities';

@Schema()
export class Candidate implements CandidateEntity {
    _id: ObjectId;
    @Prop({
        type: String,
        unique: true,
        required: true,
    })
    email: string;
    @Prop({
        required: [true, 'A name is required !'],
        validate: {
            validator: (v) => {
                return v.length >= 3 ?? true;
            },
            message: () =>
                'The name of the user must be at least 3 characters length',
        },
    })
    firstname: string;
    @Prop({
        required: [true, 'A name is required !'],
        validate: {
            validator: (v) => {
                return v.length >= 3 ?? true;
            },
            message: () =>
                'The name of the user must be at least 3 characters length',
        },
    })
    lastname: string;
    @Prop({
        type: String,
        required: false,
        unique: true,
    })
    access_token: string;
    @Prop({
        type: mongoose.Schema.Types.ObjectId,
        required: false,
        // ref: JobCategory.name,
        unique: false,
    })
    job_category?: ObjectId;
    @Prop({
        type: {
            _id: false,
            resume: { type: String, unique: false },
            linkedin: { type: String, unique: false },
            websites: { type: [String], unique: false },
        },
    })
    urls: { resume: string; linkedin: string; websites: string[] };
    @Prop({
        required: false,
        type: {
            experience: { type: Number, min: 1, max: 5 },
            skills: { type: Number, min: 1, max: 5 },
            softskills: { type: Number, min: 1, max: 5 },
        },
    })
    rating: CandidateEntity['rating'];
    @Prop({
        default: new Date(),
        required: false,
    })
    updatedAt?: Date;
    @Prop({
        default: new Date(),
        required: false,
    })
    createdAt?: Date;
}
export const CandidateSchema = SchemaFactory.createForClass(Candidate);
export type CandidateDocument = HydratedDocument<Candidate>;
