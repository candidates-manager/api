import { Field, ArgsType } from '@nestjs/graphql';
import { IsArray, IsString, ArrayMinSize } from 'class-validator';
import { CandidateArgsModel } from 'talenthive-models/lib/Entities';

@ArgsType()
export default class CandidateArgs implements CandidateArgsModel {
    @Field(() => [String], { nullable: true })
    company_id: string[];
    @Field(() => String, { nullable: true })
    @IsString()
    email: string;
}
