import { Field, ObjectType } from '@nestjs/graphql';

//Entity
import { CandidateResponseModel } from 'talenthive-models/lib/Entities';

@ObjectType()
class UrlsCandidateResponse {
    @Field()
    linkedin: string;
    @Field()
    resume: string;
    @Field(() => [String])
    websites: string[];
}
@ObjectType()
export default class CandidateResponse implements CandidateResponseModel {
    @Field()
    _id: string;
    @Field()
    email: string;
    @Field()
    job_category: string;
    @Field()
    firstname: string;
    @Field()
    lastname: string;
    @Field()
    urls?: UrlsCandidateResponse;
    @Field()
    updatedAt?: Date;
    @Field()
    createdAt?: Date;
}
