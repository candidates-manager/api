import { Field, InputType } from '@nestjs/graphql';
import {
    IsNotEmpty,
    IsEmail,
    IsString,
    IsOptional,
    IsNumber,
} from 'class-validator';
import { CandidateInputModel } from 'talenthive-models/lib/Entities';

@InputType()
export class CandidateInput implements CandidateInputModel {
    @Field()
    @IsNotEmpty()
    @IsEmail()
    email: string;
    @Field()
    @IsNotEmpty()
    @IsString()
    job_id: string;
    @Field()
    @IsNotEmpty()
    @IsString()
    firstname: string;
    @Field()
    @IsNotEmpty()
    @IsString()
    lastname: string;
    @Field()
    @IsOptional()
    @IsString()
    linkedin: string;
    @Field()
    @IsString()
    origin: string;
    @Field()
    @IsOptional()
    @IsString()
    access_token: string;
}
