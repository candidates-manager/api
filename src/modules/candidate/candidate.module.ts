import { Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { ConfigService } from '@nestjs/config';
//Controller
import CandidateController from './candidate.controller';

//Schema
import { MongooseModule } from '@nestjs/mongoose';
import { Candidate, CandidateSchema } from './candidate.schema';

//Service
import CandidateService from './candidate.service';

//Modules
import JobModule from '../job/job.module';
import ApplicationModule from '../application/application.module';
import ApplicationStepModule from '../applicationStep/applicationStep.module';

@Module({
    imports: [
        MongooseModule.forFeature([
            { name: Candidate.name, schema: CandidateSchema },
        ]),
        JobModule,
        ApplicationModule,
        ApplicationStepModule,
        JwtModule.registerAsync({
            useFactory: async (configService: ConfigService) => ({
                secret: process.env.JWT_SECRET,
            }),
            inject: [ConfigService],
        }),
    ],
    exports: [CandidateService],
    controllers: [CandidateController],
    providers: [CandidateService],
})
export default class CandidateModule {}
