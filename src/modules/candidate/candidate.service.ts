import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';

//Mongoose
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';

//Schema
import { Candidate, CandidateDocument } from './candidate.schema';

//DTO
import { CandidateInput } from './dto/candidate.input.dto';

import ApplicationServices from '@Modules/application/application.service';
import JobServices from '@Modules/job/job.service';

//Response
import JobResponseDocument from '../job/dto/job.response';

//Handle Error
import handleError from '@Application/errors/handleError';
import CandidateResponse from './dto/candidate.response.dto';
import ApplicationStepInput from '../applicationStep/dto/ApplicationStep.input.dto';
import { ApplicationStepServices } from '../applicationStep/applicationStep.service';

@Injectable()
export default class CandidateServices {
    constructor(
        @InjectModel(Candidate.name)
        private CandidateModel: Model<Candidate>,
        private readonly jwtService: JwtService,
        private readonly ApplicationServices: ApplicationServices,
        private readonly JobServices: JobServices,
        private readonly applicationStepServices: ApplicationStepServices
    ) {}

    //Find Candidates
    async getCandidates(
        params,
        projections = null,
        options = {}
    ): Promise<CandidateResponse[] | Error> {
        try {
            if (
                projections &&
                Object.keys(projections).length === 1 &&
                projections?._id &&
                projections._id === 1
            ) {
                return params;
            }
            const query = this.CandidateModel.find(params, projections);
            const response = <CandidateResponse[]>await query.lean();

            return response;
        } catch (error: unknown) {
            handleError(error, 'Cannot Find Candidates');
        }
    }

    //Get Candidate
    async getCandidate(
        params,
        projections = null
    ): Promise<CandidateResponse | Error> {
        try {
            const query = this.CandidateModel.findOne(params, projections);
            const response = <CandidateResponse>await query.lean();
            return response;
        } catch (error: unknown) {
            handleError(error, 'Cannot Find Candidate');
        }
    }

    //Count Candidates
    async count(params): Promise<number | Error> {
        try {
            return this.CandidateModel.count(params);
        } catch (error) {
            handleError(error, 'Cannot Count Candidates');
        }
    }

    //Add Candidate
    async addCandidate(
        data: CandidateInput
    ): Promise<CandidateResponse | Error> {
        try {
            //1 - Check If Candidate Exists
            let candidate = await this.getCandidate({ email: data.email });

            //2 - Check If there is an error
            if (candidate instanceof Error) throw Error('Cannot Get Candidate');

            //3 - Affect
            let candidate_id = candidate._id || null;

            //2 - The Candidate Does Not Exist - Let's create him/her
            if (!candidate) {
                //# - Create Access Token
                const access_token = await this.jwtService.sign({});
                //Create Candidate
                const newCandidate = await this.CandidateModel.create({
                    ...data,
                    access_token: access_token,
                });
                if (!newCandidate) throw Error('Cannot Create Candidate');
                candidate_id = newCandidate._id.toString();
            }

            //3 - Find Job
            const job = <JobResponseDocument>(
                await this.JobServices.getJob(
                    { _id: data.job_id },
                    {},
                    { populate: ['application_process_id'] }
                )
            );

            //4 - Get Job Process
            const process = job.application_process_id;

            //5 - Create Application
            const newApplication =
                await this.ApplicationServices.addApplication({
                    candidate_id: candidate._id.toString(),
                    origin: data.origin,
                    job_id: data.job_id,
                });
            if (newApplication instanceof Error)
                throw Error('Cannot Create Application');

            //6 - Create Application Steps
            if (process.steps) {
                const steps: ApplicationStepInput[] = [];
                process.steps.map((s) => {
                    steps.push({
                        application_id: newApplication._id,
                        step: s.step,
                        duration: s.duration,
                        description: s.description,
                        name: s.name,
                    });
                });

                const newSteps =
                    await this.applicationStepServices.addApplicationSteps(
                        steps
                    );
                if (newSteps instanceof Error)
                    throw Error('Cannot Create Application Steps');
            }

            return candidate;
        } catch (error: unknown) {
            handleError(error, 'Cannot Add Candidate');
        }
    }
}
