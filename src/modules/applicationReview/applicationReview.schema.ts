import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { HydratedDocument, Types, ObjectId } from 'mongoose';

//Schemas
import { User } from '../user/user.schema';
import { Application } from '../application/application.schema';
import { Application_Step } from '../applicationStep/applicationStep.schema';

//Entity
import { ApplicationReviewEntity } from 'talenthive-models/lib/Entities';

@Schema()
export class Application_Review implements ApplicationReviewEntity {
    _id: ObjectId;
    @Prop({
        type: Types.ObjectId,
        required: function () {
            return !this.step_id;
        },
        unique: false,
        ref: 'Application',
    })
    application_id?: Application;
    @Prop({
        type: Types.ObjectId,
        required: function () {
            return !this.application_id;
        },
        unique: false,
        ref: 'Application_Step',
    })
    step_id?: Application_Step;
    @Prop({
        type: Types.ObjectId,
        required: true,
        unique: false,
        ref: 'User',
    })
    user_id: User;

    @Prop({
        required: true,
        type: Number,
    })
    rating: Number;
    @Prop({
        required: false,
        type: String,
    })
    description: String;
    @Prop({
        default: new Date(),
        required: false,
    })
    updatedAt: Date;
    @Prop({
        default: new Date(),
        required: false,
    })
    createdAt: Date;
}
export const ApplicationReviewSchema =
    SchemaFactory.createForClass(Application_Review);
ApplicationReviewSchema.index(
    { application_id: 1, user_id: 1 },
    { unique: true }
);
ApplicationReviewSchema.index({ step_id: 1, user_id: 1 }, { unique: true });
export type ApplicationReviewDocument = HydratedDocument<Application_Review>;
