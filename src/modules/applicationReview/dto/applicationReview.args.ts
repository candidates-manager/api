import { Field, ArgsType } from '@nestjs/graphql';
import { IsOptional } from 'class-validator';

//Entities
import { ApplicationReviewArgsModel } from 'talenthive-models/lib/Entities';

@ArgsType()
export class ApplicationReviewArgs implements ApplicationReviewArgsModel {
    @Field(() => String, { nullable: true })
    @IsOptional()
    application_id: string[];
}
