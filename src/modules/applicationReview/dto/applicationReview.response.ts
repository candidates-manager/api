import { Field, ObjectType } from '@nestjs/graphql';
import { Document, Schema } from 'mongoose';

//Entities
import {
    ApplicationReviewResponseModel,
    UserEntity,
} from 'talenthive-models/lib/Entities';
import { ApplicationResponse } from '@/modules/application/dto/application.response';

//Responses
import { UserResponse } from '@/modules/user/dto/user.response';
import { IsDate } from 'class-validator';

@ObjectType()
export class ApplicationReviewResponse
    extends Document
    implements ApplicationReviewResponseModel
{
    step_id?: any;
    rating: Number;
    user_id: Schema.Types.ObjectId | UserEntity;
    @Field()
    _id: string;
    @Field(() => ApplicationResponse)
    application_id: ApplicationResponse;
    @Field(() => [UserResponse])
    users?: UserResponse[] | Schema.Types.ObjectId[];
    @Field()
    step: Number;
    @Field()
    name: String;
    @Field()
    description: String;
    @Field()
    duration: Number;
    @Field()
    @IsDate()
    createdAt: Date;
    @Field()
    @IsDate()
    updatedAt: Date;
}
