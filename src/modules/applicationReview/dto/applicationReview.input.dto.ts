import { Field, InputType } from '@nestjs/graphql';
import { IsNumber, IsString, Min, Max } from 'class-validator';

import {
    ApplicationReviewInputModel,
    UserEntity,
} from 'talenthive-models/lib/Entities';

@InputType()
export default class ApplicationReviewInput
    implements ApplicationReviewInputModel
{
    @Field(() => String)
    application_id: string;
    @Field(() => String)
    step_id: string;
    @Field(() => String)
    user_id: string;
    @Field(() => Number)
    @Min(1)
    @Max(5)
    @IsNumber()
    rating: Number;
    @Field(() => String)
    @IsString()
    description: String;
}
