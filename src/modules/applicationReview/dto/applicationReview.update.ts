import { Field, InputType } from '@nestjs/graphql';
import { IsNumber, IsString, IsOptional, IsUrl } from 'class-validator';
import { Schema } from 'mongoose';

//Entities
import {
    ApplicationReviewUpdateModel,
    UserEntity,
} from 'talenthive-models/lib/Entities';

@InputType()
export class ApplicationReviewUpdate implements ApplicationReviewUpdateModel {
    @Field(() => Number)
    @IsNumber()
    rating: Number;
    @Field(() => String)
    @IsString()
    description: String;
}
