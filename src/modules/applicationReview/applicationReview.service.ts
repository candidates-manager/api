import { Injectable } from '@nestjs/common';

//Mongoose
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';

//Schema
import {
    Application_Review,
    ApplicationReviewDocument,
} from './ApplicationReview.schema';

//DTO
import ApplicationReviewCreateDto from './dto/ApplicationReview.input.dto';
import { ApplicationReviewUpdate } from './dto/ApplicationReview.update';

//Response
import { ApplicationReviewResponse } from './dto/ApplicationReview.response';

//Handle Error
import handleError from '@/Application/errors/handleError';
@Injectable()
export class ApplicationReviewServices {
    constructor(
        @InjectModel(Application_Review.name)
        private ApplicationReviewModel: Model<Application_Review>
    ) {}

    //Add ApplicationReview
    async addApplicationReview(
        data: ApplicationReviewCreateDto
    ): Promise<ApplicationReviewDocument | Error> {
        try {
            const newApplicationReview = <ApplicationReviewDocument>(
                await this.ApplicationReviewModel.create(data)
            );

            return newApplicationReview;
        } catch (error: unknown) {
            handleError(error, 'Cannot Add ApplicationReview');
        }
    }

    //Find Application Reviews
    async getApplicationReviews(
        params,
        projections = null,
        options = {}
    ): Promise<ApplicationReviewResponse[] | Error> {
        try {
            const query = this.ApplicationReviewModel.find(params, projections);
            query.populate('user_id', 'firstname lastname avatar');
            const response = <ApplicationReviewResponse[]>await query.lean();

            return response;
        } catch (error: unknown) {
            handleError(error, 'Cannot Find Application Steps');
        }
    }

    //Update ApplicationReview
    async updateApplicationReview(
        _id: string,
        data: ApplicationReviewUpdate
    ): Promise<ApplicationReviewResponse | Error> {
        try {
            const updatedApplicationReview = <ApplicationReviewResponse>(
                await this.ApplicationReviewModel.findByIdAndUpdate(
                    { _id },
                    data,
                    {
                        new: true,
                    }
                )
            );
            return updatedApplicationReview;
        } catch (error: unknown) {
            handleError(error, 'Cannot Update Application Review');
        }
    }
}
