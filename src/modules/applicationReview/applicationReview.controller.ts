import {
    Controller,
    Body,
    Get,
    Post,
    Param,
    Query,
    Patch,
} from '@nestjs/common';

//Services
import { ApplicationReviewServices } from './applicationReview.service';

//Handle Error
import handleError from '@/Application/errors/handleError';

//Restrictions
import {
    CompanyRestriction,
    CandidateRestriction,
} from '@/Application/decorators';

//DTO
import { ApplicationReviewResponse } from './dto/ApplicationReview.response';
import { ApplicationReviewUpdate } from './dto/ApplicationReview.update';

//Guards
import { Roles } from '@/Application/decorators/roles.decorator';

//Pipes
import {
    ValidateBody,
    ValidateParamId,
    ValidationBodyApplicationReview,
} from '@/Application/pipes';
import { ApplicationReviewDocument } from './applicationReview.schema';
import { ApplicationReviewArgs } from './dto/applicationReview.args';

//Controller
@Controller('applications/:application_id/reviews')
export default class ApplicationReviewController {
    constructor(private applicationReviewServices: ApplicationReviewServices) {}

    //Find Application Reviews
    @Get()
    @Roles('recruiter', 'candidate')
    @CompanyRestriction(true)
    @CandidateRestriction(true)
    async find(
        @Param('application_id', ValidateParamId)
        application_id: string,
        @Query('step_id', ValidateParamId)
        step_id: string,
        @Query('user_id', ValidateParamId)
        user_id: string
    ): Promise<ApplicationReviewResponse[] | Error> {
        try {
            const params = { application_id };
            if (step_id) Object.assign(params, { step_id });
            if (user_id) Object.assign(params, { user_id });
            const ApplicationReview = <ApplicationReviewResponse[]>(
                await this.applicationReviewServices.getApplicationReviews(
                    params
                )
            );
            return ApplicationReview;
        } catch (error) {
            handleError(error, 'Cannot Get Application Review');
        }
    }

    //Add Application Review
    @Post('/add')
    @Roles('recruiter')
    @CompanyRestriction(true)
    async add(
        @Param('application_id', ValidateParamId)
        application_id: string,
        @Body(new ValidateBody(), new ValidationBodyApplicationReview()) body
    ): Promise<ApplicationReviewDocument | Error> {
        try {
            const newReview = <ApplicationReviewDocument>(
                await this.applicationReviewServices.addApplicationReview({
                    application_id,
                    ...body,
                })
            );
            return newReview;
        } catch (error) {
            handleError(error, 'Cannot Add Application Review');
        }
    }

    //Update Application Review
    @Patch('/u/:_id')
    @Roles('recruiter')
    @CompanyRestriction(true)
    async update(
        @Body(new ValidateBody(), new ValidationBodyApplicationReview())
        body: ApplicationReviewUpdate,
        @Param('_id', ValidateParamId)
        _id: string
    ): Promise<ApplicationReviewResponse | Error> {
        try {
            const ApplicationReview = <ApplicationReviewResponse>(
                await this.applicationReviewServices.updateApplicationReview(
                    _id,
                    body
                )
            );
            return ApplicationReview;
        } catch (error) {
            handleError(error, 'Cannot Update Application Review');
        }
    }
}
