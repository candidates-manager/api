import { Module, forwardRef } from '@nestjs/common';

//Controller
import ApplicationReviewController from './ApplicationReview.controller';

//Resolver

//Schema
import { MongooseModule } from '@nestjs/mongoose';
import {
    Application_Review,
    ApplicationReviewSchema,
} from './ApplicationReview.schema';

//Service
import { ApplicationReviewServices } from './applicationReview.service';

//Modules
import JobModule from '@Modules/job/job.module';

@Module({
    imports: [
        MongooseModule.forFeature([
            { name: Application_Review.name, schema: ApplicationReviewSchema },
        ]),
        forwardRef(() => JobModule),
    ],
    exports: [ApplicationReviewServices],
    providers: [ApplicationReviewServices],
    controllers: [ApplicationReviewController],
})
export default class ApplicationReviewModule {}
