//Nestjs
import { Controller, Post, Body, Patch, Param, Get } from '@nestjs/common';

//Service
import CompanyServices from '@Modules/company/company.service';

//DTO
import { CompanyInput } from './dto/company.input';
import { CompanyUpdate } from './dto/company.update';
import { CompanyResponse } from './dto/company.response';

//Guards
import { CompanyRestriction, Roles } from '@/application/decorators';

//Pipes
import { ValidateCompanyBody, ValidateParamId } from '@/application/pipes';

//Handle Error
import handleError from '@/application/errors/handleError';
import { CompanyDocument } from './company.schema';
@Controller('company')
export default class CompanyController {
    constructor(private readonly CompanyServices: CompanyServices) {}
    @Post('/add')
    @Roles('admin')
    async add(
        @Body(new ValidateCompanyBody()) body: CompanyInput
    ): Promise<CompanyDocument | Error> {
        try {
            return await this.CompanyServices.addCompany(body);
        } catch (error) {
            handleError(error, 'Cannot Add Job');
        }
    }

    //Get Company
    @Get('/v/:company_id')
    @Roles('recruiter')
    @CompanyRestriction(true)
    async get(
        @Param('company_id', new ValidateParamId())
        company_id: string
    ): Promise<CompanyResponse | Error> {
        try {
            const company = <CompanyResponse>(
                await this.CompanyServices.getCompany({ _id: company_id }, {})
            );
            return company;
        } catch (error) {
            handleError(error, 'Cannot Get Company');
        }
    }

    //Update Company
    @Patch('/u/:_id')
    @Roles('recruiter')
    @CompanyRestriction(true)
    async update(
        @Body(new ValidateCompanyBody()) body: CompanyUpdate,
        @Param('_id', ValidateParamId)
        _id: string
    ): Promise<CompanyResponse | Error> {
        try {
            return await this.CompanyServices.updateCompany(_id, body);
        } catch (error) {
            handleError(error, 'Cannot Update Company');
        }
    }
}
