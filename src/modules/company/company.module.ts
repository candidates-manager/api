import { Module, forwardRef } from '@nestjs/common';

//Controller
import CompanyController from './company.controller';

//Resolver
import CompanyResolver from './company.resolver';

//Service
import CompanyService from './company.service';

//Modules
import JobModule from '@Modules/job/job.module';
import ApplicationProcessModule from '../applicationProcess/applicationProcess.module';

//Schema
import { MongooseModule } from '@nestjs/mongoose';
import { Company, CompanySchema } from './company.schema';

@Module({
    imports: [
        MongooseModule.forFeature([
            { name: Company.name, schema: CompanySchema },
        ]),
        JobModule,
        ApplicationProcessModule,
    ],
    exports: [CompanyService],
    controllers: [CompanyController],
    providers: [CompanyResolver, CompanyService],
})
export default class CompanyModule {}
