import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { HydratedDocument, Types, ObjectId } from 'mongoose';

//Entity
import { CompanyEntity } from 'talenthive-models/lib/Entities';

@Schema()
export class Company implements CompanyEntity {
    _id: ObjectId;
    @Prop({
        required: [true, 'A name is required !'],
        unique: false,
        validate: {
            validator: (v) => {
                return v.length >= 3 ?? true;
            },
            message: () =>
                'The name of the company must be at least 3 characters length',
        },
    })
    name: string;
    @Prop({
        type: {
            _id: false,
            glassdoor: { type: String, unique: false },
            linkedin: { type: String, unique: false },
        },
    })
    urls: { glassdoor: string; linkedin: string };
}
export const CompanySchema = SchemaFactory.createForClass(Company);
export type CompanyDocument = HydratedDocument<Company>;
