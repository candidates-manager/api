import { Field, InputType } from '@nestjs/graphql';
import { CompanyInputModel } from 'talenthive-models/lib/Entities';

@InputType()
export class CompanyInput implements CompanyInputModel {
    @Field()
    name: string;
}
