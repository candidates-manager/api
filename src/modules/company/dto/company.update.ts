import { Field, InputType } from '@nestjs/graphql';
import { IsOptional, IsUrl } from 'class-validator';
import { CompanyUpdateModel } from 'talenthive-models/lib/Entities';

@InputType()
export class CompanyUpdate implements CompanyUpdateModel {
    @Field()
    name: string;
    @Field()
    linkedin?: string;
    @Field()
    glassdoor?: string;
}
