import { Field, ObjectType } from '@nestjs/graphql';
import { Document } from 'mongoose';

//Entity

//Resolve Fields
import JobResponse from '@Modules/job/dto/job.response';
import { CompanyResponseModel } from 'talenthive-models/lib/Entities';
import { IsOptional } from 'class-validator';

@ObjectType()
class CompanyUrlsEntity {
    @Field()
    linkedin: string;
    @Field()
    glassdoor: string;
}

@ObjectType()
export class CompanyResponse extends Document implements CompanyResponseModel {
    @Field()
    @IsOptional()
    _id: string;
    @Field()
    @IsOptional()
    name: string;
    @Field()
    @IsOptional()
    urls: CompanyUrlsEntity;
    @Field()
    @IsOptional()
    updatedAt?: Date;
    @Field()
    @IsOptional()
    createdAt?: Date;
    @Field(() => [JobResponse], { nullable: true })
    @IsOptional()
    jobs: JobResponse[];
}
