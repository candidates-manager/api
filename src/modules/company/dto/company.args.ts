import { Field, ArgsType } from '@nestjs/graphql';
import { IsOptional } from 'class-validator';

//Entities
import { CompanyArgsModel } from 'talenthive-models/lib/Entities';

@ArgsType()
export default class CompanyArgs implements CompanyArgsModel {
    @Field(() => [String], { nullable: true })
    @IsOptional()
    company_id?: [String];
}
