//NestJs
import { UseGuards } from '@nestjs/common';

//Graphql
import { Resolver, Query, Args, ResolveField, Parent } from '@nestjs/graphql';

//Company - Service
import CompanyService from './company.service';

//Company - DTO
import { CompanyResponse } from './dto/company.response';
import CompanyArgs from './dto/company.args';

// Job - Service
import JobServices from '../job/job.service';

//Job - DTO
import JobResponse from '../job/dto/job.response';

//Decorators
import { Projections } from '@/application/decorators';
//Restrictions
import { Roles, CompanyRestriction } from '@/application/decorators';

//Pipes
import { ValidateArgs } from '@Pipes/validate.args.pipe';

//Guards
import { AuthJWTGuard, CompanyGuard } from '@Application/guards';

//Handle Error
import handleError from '@/application/errors/handleError';

@Resolver(() => CompanyResponse)
@UseGuards(AuthJWTGuard)
@UseGuards(CompanyGuard)
export default class CompanyResolver {
    constructor(
        private readonly CompanyService: CompanyService,
        private readonly JobServices: JobServices
    ) {}

    //Get Company
    @Query(() => [CompanyResponse])
    @Roles('recruiter')
    @CompanyRestriction(true)
    async companies(
        @Args(new ValidateArgs()) args: CompanyArgs,
        @Projections() projections: any
    ): Promise<CompanyResponse[] | Error> {
        try {
            const companies = <CompanyResponse[]>(
                await this.CompanyService.findCompanies(
                    {
                        _id: args.company_id,
                    },
                    projections
                )
            );
            return companies;
        } catch (error) {
            handleError(error, 'Cannot Get Companies');
        }
    }

    //Jobs
    @ResolveField(() => [JobResponse])
    async jobs(
        @Parent() company: CompanyResponse,
        @Projections() projections: any
    ): Promise<[JobResponse] | Error> {
        try {
            return <[JobResponse]>await this.JobServices.getJobs({
                company_id: [company._id],
            });
        } catch (error) {
            handleError(error, 'Cannot Get Jobs');
        }
    }
}
