import { Injectable, Inject, forwardRef } from '@nestjs/common';

//Document
import { CompanyEntity } from 'talenthive-models/lib/Entities';

//Mongoose
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import mongoose from 'mongoose';

//Schema
import { Company, CompanyDocument } from './company.schema';

//DTO
import { CompanyInput } from './dto/company.input';

//Handle Error
import handleError from '@/application/errors/handleError';
import { CompanyUpdate } from './dto/company.update';
import { CompanyResponse } from './dto/company.response';

@Injectable()
export default class CompanyServices {
    constructor(
        @InjectModel(Company.name)
        private CompanyModel: Model<Company>
    ) {}

    //Find Companys
    async findCompanies(
        params,
        projections,
        options = {}
    ): Promise<CompanyEntity[] | undefined | Error> {
        try {
            if (
                projections &&
                Object.keys(projections).length === 1 &&
                projections?._id &&
                projections._id === 1
            ) {
                return params;
            }
            const query = this.CompanyModel.find(params, projections);
            const response = <CompanyEntity[]>await query.lean();

            return response;
        } catch (error) {
            handleError(error, 'Cannot Find Companies');
        }
    }

    //Add Company
    async addCompany(data: CompanyInput): Promise<CompanyDocument | Error> {
        try {
            const newCompany = <CompanyDocument>(
                await this.CompanyModel.create(data)
            );
            return newCompany;
        } catch (error) {
            handleError(error, 'Cannot Add Company');
        }
    }

    //Get Job
    async getCompany(
        params,
        projections = null
    ): Promise<CompanyResponse | Error> {
        try {
            const query = this.CompanyModel.findOne(params, projections);
            const response = <CompanyResponse>await query.lean();
            if (!response) throw new Error();
            return response;
        } catch (error: unknown) {
            handleError(error, 'Cannot Find Company');
        }
    }

    //Update User
    async updateCompany(
        _id: string,
        data: CompanyUpdate
    ): Promise<CompanyResponse | Error> {
        try {
            const updatedCompany = <CompanyResponse>(
                await this.CompanyModel.findByIdAndUpdate({ _id }, data, {
                    new: true,
                })
            );
            return updatedCompany;
        } catch (error: unknown) {
            handleError(error, 'Cannot Update Company');
        }
    }

    //Count Company
    async count(params): Promise<number | Error> {
        try {
            return this.CompanyModel.count(params);
        } catch (error) {
            handleError(error, 'Cannot Count Companies');
        }
    }

    //Delete Company
    async deleteCompany(params): Promise<Boolean | undefined | Error> {
        try {
            const deletion = await this.CompanyModel.deleteOne(params);
            return true;
        } catch (error) {
            handleError(error, 'Cannot Delete Company');
        }
    }
}
