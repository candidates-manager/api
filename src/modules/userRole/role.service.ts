import { Injectable, Inject, forwardRef } from '@nestjs/common';

//Mongoose
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';

//DTO
import { RoleInput } from './dto/role.input';
import { RoleResponse } from './dto/role.response';
import { RoleArgs } from './dto/role.args';

//Schema
import { Role, RoleDocument } from './role.schema';

//Handle Error
import handleError from '@/application/errors/handleError';

@Injectable()
export class RoleServices {
    constructor(
        @InjectModel(Role.name)
        private RoleModel: Model<Role>
    ) {}

    //Add Role
    async addRole(data: RoleInput): Promise<RoleDocument | Error> {
        try {
            const newRole = <RoleDocument>await this.RoleModel.create(data);
            return newRole;
        } catch (error) {
            handleError(error, 'Cannot Add Role');
        }
    }

    //Get Roles
    async getRoles(
        params: RoleArgs,
        projections
    ): Promise<RoleResponse[] | undefined> {
        try {
            const query = this.RoleModel.find(params, projections);
            const response = <RoleResponse[]>await query.lean();
            return response;
        } catch (error) {
            handleError(error, 'Cannot Find Roles');
        }
    }
}
