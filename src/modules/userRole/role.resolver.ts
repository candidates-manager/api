//Graphql
import { Resolver, Query, Args, ResolveField, Parent } from '@nestjs/graphql';

//Module
import { Role } from './role.schema';

//Services
import { RoleServices } from './role.service';
import UserServices from '@Modules/user/user.service';
//DTO
import { RoleResponse } from './dto/role.response';
import { RoleArgs } from './dto/role.args';
import { UserResponse } from '../user/dto/user.response';
import { UserArgs } from '../user/dto/user.args';

//Decorators
import {
    Roles,
    Projections,
    CompanyRestriction,
} from '@/application/decorators';

//Pipes
import { ValidateArgs } from '@Pipes/validate.args.pipe';

//Functions
import handleError from '@/application/errors/handleError';

@Resolver(() => RoleResponse)
export class RoleResolver {
    constructor(
        private readonly UserServices: UserServices,
        private readonly RoleServices: RoleServices
    ) {}

    //Get Roles
    @Query(() => [RoleResponse])
    @Roles('admin', 'recruiter')
    @CompanyRestriction(true)
    async roles(
        @Args(new ValidateArgs()) args: RoleArgs,
        @Projections() projections
    ) {
        try {
            const roles = <RoleResponse[]>(
                await this.RoleServices.getRoles(args, projections)
            );
            return roles;
        } catch (error) {
            handleError(error, 'Cannot Get Roles');
        }
    }

    //Resolver - Applications
    @ResolveField(() => [UserResponse])
    async users(
        @Parent() role: RoleResponse,
        @Args(new ValidateArgs()) args: UserArgs,
        @Projections() projections: any
    ): Promise<UserResponse[] | Error> {
        try {
            const params = {
                ...args,
                role_id: role._id,
            };
            return <UserResponse[]>(
                await this.UserServices.getUsers(params, projections)
            );
        } catch (error) {
            handleError(error, 'Cannot Get Users');
        }
    }
}
