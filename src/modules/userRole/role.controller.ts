import { Controller, Body, Post, Get, Param, Query } from '@nestjs/common';

//Service
import { RoleServices } from './role.service';

//Decorator
import { Roles } from '@/application/decorators';

//Pipes
import { ValidateBody } from '@/application/pipes';

//DTO
import { RoleInput } from './dto/role.input';

//Handle Error
import handleError from '@/application/errors/handleError';
import { RoleResponse } from './dto/role.response';
@Controller('roles')
export default class RoleController {
    constructor(private readonly RoleServices: RoleServices) {}

    //Get Roles
    @Get('/')
    @Roles('admin', 'recruiter')
    async getRoles(
        @Query('public_id')
        public_id: string[]
    ): Promise<RoleResponse[] | Error> {
        try {
            return <RoleResponse[]>(
                await this.RoleServices.getRoles({ public_id }, {})
            );
        } catch (error) {
            handleError(error, 'Cannot create Role');
        }
    }

    //Add Role
    @Post('/add')
    @Roles('admin')
    async register(
        @Body(new ValidateBody()) body: RoleInput
    ): Promise<void | Error> {
        try {
            this.RoleServices.addRole(body);
        } catch (error) {
            handleError(error, 'Cannot create Role');
        }
    }
}
