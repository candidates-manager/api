import { Field, InputType } from '@nestjs/graphql';

@InputType()
export class RoleInput {
    @Field()
    public_id: string;
    @Field()
    name: string;
}
