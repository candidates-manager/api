import { UserResponse } from '@/modules/user/dto/user.response';
import { Field, ObjectType } from '@nestjs/graphql';
import { Document } from 'mongoose';

import { RoleResponseModel } from 'talenthive-models/lib/entities/user/Role.Entity';
import { RoleEnum } from 'talenthive-models/lib/Enums';

@ObjectType()
export class RoleResponse extends Document implements RoleResponseModel {
    @Field()
    _id: string;
    @Field()
    public_id: string;
    @Field()
    name: RoleEnum;
    @Field(() => [UserResponse], { nullable: true })
    users: UserResponse[];
}
