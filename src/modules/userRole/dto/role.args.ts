import { Field, ArgsType } from '@nestjs/graphql';
import { IsOptional } from 'class-validator';
import { RoleArgsModel } from 'talenthive-models/lib/Entities';

@ArgsType()
export class RoleArgs implements RoleArgsModel {
    @Field(() => [String], { nullable: true })
    @IsOptional()
    public_id?: string[];
}
