import { Module, forwardRef } from '@nestjs/common';
import { RoleServices } from './role.service';

//Schema
import { MongooseModule } from '@nestjs/mongoose';
import { Role, RoleSchema } from './role.schema';

//Resolver
import { RoleResolver } from '@Modules/userRole/role.resolver';

//Controller
import RoleController from './role.controller';

//Dependencies
import UserModule from '../user/user.module';

@Module({
    imports: [
        MongooseModule.forFeature([{ name: Role.name, schema: RoleSchema }]),
        forwardRef(() => UserModule),
    ],
    controllers: [RoleController],
    providers: [RoleResolver, RoleServices],
    exports: [RoleServices],
})
export default class RolesModule {}
