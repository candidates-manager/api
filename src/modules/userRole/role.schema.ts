import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { HydratedDocument, Types, ObjectId } from 'mongoose';

//Entity
import { RoleEntity } from 'talenthive-models/lib/Entities';
import { RoleEnum } from 'talenthive-models/lib/Enums';

export type RoleDocument = HydratedDocument<Role>;

@Schema()
export class Role implements RoleEntity {
    _id: ObjectId;
    @Prop({
        required: [true, 'A public ID is required !'],
        unique: true,
    })
    public_id: string;
    @Prop({
        required: [true, 'A name is required !'],
        unique: true,
        validate: {
            validator: (v) => {
                return v.length >= 3 ?? true;
            },
            message: () =>
                'The name of the role must be at least 3 characters length',
        },
    })
    name: RoleEnum;
}
export const RoleSchema = SchemaFactory.createForClass(Role);
