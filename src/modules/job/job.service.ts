import { Injectable } from '@nestjs/common';

//Mongoose
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';

//Schema
import { Job } from './job.schema';

//DTO
import JobArgs from './dto/job.args';
import { JobInput } from './dto/job.input.dto';
import { JobUpdate } from './dto/job.update.dto';
import JobResponse from './dto/job.response';
//Models
import { JobEntity } from 'talenthive-models/lib/Entities';

//Handle Error
import handleError from '@/application/errors/handleError';

@Injectable()
export default class JobServices {
    constructor(
        @InjectModel(Job.name)
        private JobModel: Model<Job>
    ) {}

    //Get Jobs
    async getJobs(
        params: JobArgs,
        projections = null,
        options = {}
    ): Promise<[JobResponse] | Error> {
        try {
            const query = this.JobModel.find(params, projections);
            query.populate('application_process_id');
            const response = <[JobResponse]>await query.lean();
            return response;
        } catch (error: unknown) {
            handleError(error, 'Cannot Find Jobs');
        }
    }

    //Add Job
    async addJob(data: JobInput): Promise<JobEntity | Error> {
        try {
            const createdJob = <JobEntity>await this.JobModel.create(data);
            return createdJob;
        } catch (error: unknown) {
            handleError(error, 'Cannot Add Job');
        }
    }

    //Update Job
    async updateJob(
        _id: string,
        data: JobUpdate
    ): Promise<JobResponse | Error> {
        try {
            const updatedJob = <JobResponse>(
                await this.JobModel.findByIdAndUpdate({ _id }, data, {
                    new: true,
                })
            );

            return updatedJob;
        } catch (error: unknown) {
            handleError(error, 'Cannot Add Job');
        }
    }

    //Get Job
    async getJob(
        params,
        projections = null,
        options
    ): Promise<JobResponse | Error> {
        try {
            const query = this.JobModel.findOne(params, projections);
            // if (options.populate) {
            //     if (options.populate.includes('application_process_id')) {
            query.populate('application_process_id');
            //     }
            // }
            const response = <JobResponse>await query.lean();

            if (!response) throw new Error();

            return response;
        } catch (error: unknown) {
            handleError(error, 'Cannot Find Job');
        }
    }

    //Count Job
    async count(params): Promise<number | Error> {
        try {
            return this.JobModel.count(params);
        } catch (error) {
            handleError(error, 'Cannot Count Jobs');
        }
    }
}
