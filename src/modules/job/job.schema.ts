import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { HydratedDocument, Types, ObjectId } from 'mongoose';

//Entity
import { JobEntity } from 'talenthive-models/lib/Entities';
import { Company } from '../company/company.schema';
import { Application_Process } from '../applicationProcess/applicationprocess.schema';

//Entity
import JobStatusEnum from 'talenthive-models/lib/enums/Job.Status.Enum';

@Schema()
export class Job implements JobEntity {
    _id: ObjectId;
    @Prop({
        type: String,
        required: [true, 'A name is required !'],
        unique: false,
        validate: {
            validator: (v) => {
                return v.length >= 3 ?? true;
            },
            message: () =>
                'The name of the job must be at least 3 characters length',
        },
    })
    name: string;
    @Prop({
        type: Types.ObjectId,
        required: [true, 'A Company is required !'],
        unique: false,
        ref: 'Company',
    })
    company_id: Company;
    @Prop({
        type: Types.ObjectId,
        required: false,
        unique: false,
        ref: 'Application_Process',
    })
    application_process_id: Application_Process;
    @Prop({
        unique: false,
        default: 'published',
    })
    status: JobStatusEnum;
    @Prop({
        type: String,
        unique: false,
    })
    link: string;
    @Prop({
        default: new Date(),
        required: false,
    })
    updatedAt?: Date;
    @Prop({
        default: new Date(),
        required: false,
    })
    createdAt?: Date;
}
export const JobSchema = SchemaFactory.createForClass(Job);
export type JobDocument = HydratedDocument<Job>;
