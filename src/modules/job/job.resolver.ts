//Graphql
import { Resolver, Query, Args, ResolveField, Parent } from '@nestjs/graphql';

//Module
import JobService from './job.service';
import JobResponse from './dto/job.response';
import JobArgs from './dto/job.args';

//Decorators
import { Projections } from '@/application/decorators';

//Handle Error
import handleError from '@/application/errors/handleError';

//Application
import ApplicationServices from '../application/application.service';
import { ApplicationResponse } from '../application/dto/application.response';
import ApplicationArgs from '../application/dto/application.args';

//Restrictions
import { Roles, CompanyRestriction } from '@/application/decorators';

//Pipes
import { ValidateArgs } from '@Pipes/validate.args.pipe';

@Resolver(() => JobResponse)
export default class JobResolver {
    //Dependencies
    constructor(
        private readonly JobService: JobService,
        private readonly ApplicationServices: ApplicationServices
    ) {}

    //Query - Jobs
    @Query(() => [JobResponse])
    @Roles('recruiter')
    @CompanyRestriction(true)
    async jobs(
        @Args(new ValidateArgs()) args: JobArgs,
        @Projections() projections
    ) {
        try {
            const jobs: [JobResponse] = <[JobResponse]>(
                await this.JobService.getJobs(args, projections)
            );
            return jobs;
        } catch (error) {
            handleError(error, 'Cannot Find Jobs');
        }
    }

    //Resolver - Applications
    @ResolveField(() => [ApplicationResponse])
    async applications(
        @Parent() job: JobResponse,
        @Args(new ValidateArgs()) args: ApplicationArgs,
        @Projections() projections: any
    ): Promise<ApplicationResponse[] | Error> {
        try {
            return <ApplicationResponse[]>(
                await this.ApplicationServices.getApplications(
                    { ...args, job_id: job._id },
                    projections
                )
            );
        } catch (error) {
            handleError(error, 'Cannot Get Applications');
        }
    }
}
