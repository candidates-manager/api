import { Field, ArgsType } from '@nestjs/graphql';
import { IsOptional, IsArray, IsString, ArrayMinSize } from 'class-validator';
//Entities
import { JobArgsModel } from 'talenthive-models/lib/Entities';
import StatusEnum from 'talenthive-models/lib/enums/Job.Status.Enum';

@ArgsType()
export default class JobArgs implements JobArgsModel {
    @Field(() => String, { nullable: true })
    @IsOptional()
    _id?: string;
    @Field(() => [String], { nullable: false })
    @IsArray()
    @IsString({ each: true })
    @ArrayMinSize(1)
    company_id: string[];
    @Field(() => [String], { nullable: true })
    status?: [StatusEnum];
}
