import { Field, InputType } from '@nestjs/graphql';
import { IsOptional, IsUrl } from 'class-validator';
import { JobUpdateModel } from 'talenthive-models/lib/Entities';
import StatusEnum from 'talenthive-models/lib/enums/Job.Status.Enum';

@InputType()
export class JobUpdate implements JobUpdateModel {
    @Field()
    @IsOptional()
    name?: string;
    @Field()
    @IsOptional()
    application_process_id?: string;
    @Field()
    @IsOptional()
    status?: StatusEnum;
    @Field(() => String)
    @IsUrl()
    @IsOptional()
    link?: string;
}
