import { Field, ObjectType } from '@nestjs/graphql';
import { Document } from 'mongoose';

//Entity
import {
    JobResponseModel,
    ApplicationResponseModel,
    ApplicationProcessResponseModel,
} from 'talenthive-models/lib/Entities';

import { ApplicationResponse } from '@/modules/application/dto/application.response';
import { ApplicationProcessResponse } from '@/modules/applicationProcess/dto/applicationProcess.response';
import StatusEnum from 'talenthive-models/lib/enums/Job.Status.Enum';
import { CompanyResponse } from '@/modules/company/dto/company.response';

@ObjectType()
export default class JobResponseDocument
    extends Document
    implements JobResponseModel
{
    @Field()
    _id: string;
    @Field()
    name: string;
    @Field({ nullable: true })
    status: StatusEnum;
    @Field({ nullable: true })
    createdAt: Date;
    @Field(() => String)
    company_id: CompanyResponse;
    @Field(() => String)
    link?: string;
    @Field(() => ApplicationProcessResponse, { nullable: true })
    application_process_id: ApplicationProcessResponseModel;
    @Field(() => [ApplicationResponse], { nullable: true })
    applications?: ApplicationResponseModel[];
}
