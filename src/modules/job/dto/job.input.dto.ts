import { Field, InputType } from '@nestjs/graphql';
import { IsOptional, IsString, IsUrl } from 'class-validator';
import { JobInputModel } from 'talenthive-models/lib/Entities';

@InputType()
export class JobInput implements JobInputModel {
    @Field()
    @IsString()
    name: string;
    @Field(() => String)
    company_id: string;
    @Field(() => String)
    application_process_id: string;
    @Field(() => String)
    @IsUrl()
    @IsOptional()
    link?: string;
}
