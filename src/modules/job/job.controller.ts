import { Controller, Body, Post, Param, Get, Patch, Req } from '@nestjs/common';

//Response
import JobResponse from './dto/job.response';

//Services
import JobServices from '@Modules/job/job.service';

//DTO
import { JobInput } from './dto/job.input.dto';
import { JobUpdate } from './dto/job.update.dto';

//Guards
import { Roles, CompanyRestriction } from '@/application/decorators';

//Pipes
import { ValidateParamId, ValidateBody } from '@/application/pipes';

//Handle Error
import handleError from '@/application/errors/handleError';
import { JobEntity } from 'talenthive-models/lib/Entities';

@Controller('jobs')
export default class JobController {
    constructor(private readonly JobServices: JobServices) {}

    //Add Job
    @Post('/add')
    @Roles('recruiter')
    @CompanyRestriction(true)
    async add(
        @Body(new ValidateBody()) body: JobInput
    ): Promise<JobEntity | Error> {
        try {
            return <JobEntity>await this.JobServices.addJob(body);
        } catch (error) {
            handleError(error, 'Cannot Add Job');
        }
    }

    //Update Job
    @Patch('/u/:_id')
    @Roles('recruiter')
    @CompanyRestriction(true)
    async update(
        @Body(new ValidateBody()) body: JobUpdate,
        @Param('_id', ValidateParamId)
        _id: string,
        @Req() req
    ): Promise<JobResponse | Error> {
        try {
            const job = <JobResponse>(
                await this.JobServices.getJob({ _id }, {}, {})
            );
            return await this.JobServices.updateJob(job._id, body);
        } catch (error) {
            handleError(error, 'Cannot Update Job');
        }
    }

    //Get Job
    @Get('/v/:job_id')
    @Roles('recruiter')
    @CompanyRestriction(true)
    async get(
        @Param('job_id', new ValidateParamId())
        job_id: string
    ): Promise<JobResponse | Error> {
        try {
            const job = <JobResponse>(
                await this.JobServices.getJob({ _id: job_id }, {}, {})
            );
            return job;
        } catch (error) {
            handleError(error, 'Cannot Get Job');
        }
    }
}
