//NestJs
import { Module, forwardRef } from '@nestjs/common';

//Controller
import JobController from './job.controller';

//Resolver
import JobResolver from './job.resolver';

//Schema
import { MongooseModule } from '@nestjs/mongoose';
import { Job, JobSchema } from './job.schema';

//Service
import JobService from './job.service';

//Modules
import ApplicationModule from '../application/application.module';

@Module({
    imports: [
        MongooseModule.forFeature([{ name: Job.name, schema: JobSchema }]),
        ApplicationModule,
    ],
    exports: [JobService],
    controllers: [JobController],
    providers: [JobResolver, JobService],
})
export default class JobModule {}
