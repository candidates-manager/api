import { Module } from '@nestjs/common';
//Controller
import ReplyController from './reply.controller';

//Schema
import { MongooseModule } from '@nestjs/mongoose';
import { Reply, ReplySchema } from './reply.schema';

//Service
import ReplyService from './reply.service';

//Modules
import ApplicationModule from '../application/application.module';

@Module({
    imports: [
        MongooseModule.forFeature([{ name: Reply.name, schema: ReplySchema }]),
        ApplicationModule,
    ],
    exports: [ReplyService],
    controllers: [ReplyController],
    providers: [ReplyService],
})
export default class ReplyModule {}
