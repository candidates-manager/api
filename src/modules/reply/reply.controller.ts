import { Controller, Body, Post, Get, UseGuards, Param } from '@nestjs/common';

//Service
import ReplyServices from '@Modules/reply/reply.service';

//Restrictions
import {
    Roles,
    CompanyRestriction,
    CandidateRestriction,
} from '@/application/decorators';

//DTO
import { ReplyInput } from './dto/reply.input.dto';

//Handle Error
import handleError from '@/application/errors/handleError';

//Pipes
import { ValidateParamId, ValidateBody } from '@/application/pipes';

//Entities
import { ReplyEntity } from 'talenthive-models/lib/Entities';
import ReplyResponse from './dto/reply.response.dto';
import { ReplyDocument } from './reply.schema';

//Controller
@Controller('replies')
export default class ReplyController {
    constructor(private readonly ReplyServices: ReplyServices) {}

    //Add Reply
    @Post('/add')
    @Roles('recruiter', 'candidate', 'employee')
    @CompanyRestriction(true)
    @CandidateRestriction(true)
    async add(
        @Body(new ValidateBody()) body: ReplyInput
    ): Promise<ReplyDocument | Error> {
        try {
            const newReply = <ReplyDocument>(
                await this.ReplyServices.addReply(body)
            );
            if (!newReply) throw Error();
            return newReply;
        } catch (error) {
            handleError(error, 'Cannot create Reply');
        }
    }

    //Get Replies
    @Get('/v/:entity_type/:entity_id')
    @Roles('recruiter', 'candidate', 'employee')
    @CompanyRestriction(true)
    @CandidateRestriction(true)
    async get(
        @Param('entity_type')
        entity_type: string,
        @Param('entity_id', ValidateParamId)
        entity_id: string
    ): Promise<ReplyResponse[] | Error> {
        try {
            const replies = <ReplyResponse[]>(
                await this.ReplyServices.getReplies({
                    entity_type,
                    entity_id,
                })
            );
            if (!replies) throw Error();
            return replies;
        } catch (error) {
            handleError(error, 'Cannot find replies');
        }
    }
}
