import { Injectable } from '@nestjs/common';

//Mongoose
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';

//Schema
import { Reply, ReplyDocument } from './reply.schema';

//DTO
import { ReplyInput } from './dto/reply.input.dto';
import ReplyResponse from './dto/reply.response.dto';
import ReplyArgs from './dto/reply.args.dto';

//Handle Error
import handleError from '@Application/errors/handleError';

@Injectable()
export default class ReplyServices {
    constructor(
        @InjectModel(Reply.name)
        private ReplyModel: Model<Reply>
    ) {}

    //Add Reply
    async addReply(data: ReplyInput): Promise<ReplyDocument | Error> {
        try {
            const newReply = <ReplyDocument>(
                await this.ReplyModel.create(data).then((t) =>
                    t.populate('user_id')
                )
            );
            return newReply;
        } catch (error: unknown) {
            handleError(error, 'Cannot Add Reply');
        }
    }

    //Get Replies
    async getReplies(
        params: ReplyArgs,
        projections = null,
        options = {}
    ): Promise<ReplyResponse[] | Error> {
        try {
            const query = this.ReplyModel.find(params, projections);
            query.populate('user_id');
            query.sort({ createdAt: -1 });
            const response = <ReplyResponse[]>await query.lean();
            return response;
        } catch (error: unknown) {
            handleError(error, 'Cannot Find Replies');
        }
    }
}
