import { Schema, SchemaFactory, Prop } from '@nestjs/mongoose';
import { HydratedDocument, Types, ObjectId } from 'mongoose';

//Entity
import { ReplyEntity, UserEntity } from 'talenthive-models/lib/Entities';
import { ReplyTypeEnum } from 'talenthive-models/lib/Enums';

@Schema()
export class Reply implements ReplyEntity {
    _id: ObjectId;
    @Prop({
        type: Types.ObjectId,
        required: true,
        unique: false,
    })
    entity_id: ObjectId;
    @Prop({
        required: true,
        unique: false,
    })
    entity_type: ReplyTypeEnum;
    @Prop({
        type: Types.ObjectId,
        required: true,
        unique: false,
        ref: 'User',
    })
    user_id: ObjectId;
    @Prop({
        required: true,
    })
    content: string;
    @Prop({
        type: Types.ObjectId,
        required: false,
        ref: 'Reply',
    })
    parent_id?: ObjectId;
    @Prop({
        default: new Date(),
        required: false,
    })
    createdAt: Date;
    @Prop({
        default: new Date(),
        required: false,
    })
    updatedAt: Date;
}
export const ReplySchema = SchemaFactory.createForClass(Reply);
export type ReplyDocument = HydratedDocument<Reply>;
