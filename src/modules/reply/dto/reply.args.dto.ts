import { Field, ArgsType } from '@nestjs/graphql';
import { IsString, IsNotEmpty } from 'class-validator';
import { ReplyArgsModel } from 'talenthive-models/lib/Entities';
import { ReplyTypeEnum } from 'talenthive-models/lib/Enums';

@ArgsType()
export default class ReplyArgs implements ReplyArgsModel {
    @Field()
    @IsString()
    @IsNotEmpty()
    entity_type: string;
    @Field()
    @IsString()
    @IsNotEmpty()
    entity_id: string;
}
