import { Field, InputType } from '@nestjs/graphql';
import { ReplyTypeEnum } from 'talenthive-models/lib/Enums';
import { IsNotEmpty, IsString, IsOptional } from 'class-validator';
import { ReplyInputModel } from 'talenthive-models/lib/Entities';

@InputType()
export class ReplyInput implements ReplyInputModel {
    @Field()
    @IsString()
    @IsNotEmpty()
    entity_id: string;
    @Field(() => ReplyTypeEnum)
    @IsString()
    @IsNotEmpty()
    entity_type: ReplyTypeEnum;
    @Field()
    @IsString()
    @IsNotEmpty()
    user_id: string;
    @Field()
    @IsString()
    @IsNotEmpty()
    content: string;
    @Field()
    @IsString()
    @IsOptional()
    parent_id?: string;
}
