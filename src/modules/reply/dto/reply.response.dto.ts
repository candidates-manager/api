import { Field, ObjectType } from '@nestjs/graphql';
import { Schema } from 'mongoose';

//Entity
import {
    ReplyEntity,
    ReplyResponseModel,
    UserEntity,
} from 'talenthive-models/lib/Entities';
import { ReplyTypeEnum } from 'talenthive-models/lib/Enums';

@ObjectType()
export default class ReplyResponse implements ReplyResponseModel {
    @Field(() => Schema.Types.ObjectId)
    _id: Schema.Types.ObjectId;
    @Field(() => Schema.Types.ObjectId)
    entity_id: Schema.Types.ObjectId;
    @Field(() => ReplyTypeEnum)
    entity_type: ReplyTypeEnum;
    @Field(() => Schema.Types.ObjectId)
    parent_id?: Schema.Types.ObjectId;
    @Field(() => Schema.Types.ObjectId)
    user_id: Schema.Types.ObjectId;
    @Field()
    content: string;
    @Field()
    updatedAt: Date;
    @Field()
    createdAt: Date;
}
