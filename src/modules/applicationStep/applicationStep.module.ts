import { Module, forwardRef } from '@nestjs/common';

//Controller
import ApplicationStepController from './ApplicationStep.controller';

//Resolver

//Schema
import { MongooseModule } from '@nestjs/mongoose';
import {
    Application_Step,
    ApplicationStepSchema,
} from './ApplicationStep.schema';

//Service
import { ApplicationStepServices } from './applicationStep.service';

//Modules
import JobModule from '@Modules/job/job.module';

@Module({
    imports: [
        MongooseModule.forFeature([
            { name: Application_Step.name, schema: ApplicationStepSchema },
        ]),
        forwardRef(() => JobModule),
    ],
    exports: [ApplicationStepServices],
    providers: [ApplicationStepServices],
    controllers: [ApplicationStepController],
})
export default class ApplicationStepModule {}
