import { Field, ArgsType } from '@nestjs/graphql';
import { IsOptional } from 'class-validator';

//Entities
import { ApplicationStepArgsModel } from 'talenthive-models/lib/Entities';

@ArgsType()
export class ApplicationStepArgs implements ApplicationStepArgsModel {
    @Field(() => [String], { nullable: true })
    @IsOptional()
    _id: string;
}
