import { Field, ObjectType } from '@nestjs/graphql';
import { Document, Schema } from 'mongoose';
import { IsDate } from 'class-validator';

//Entities
import { ApplicationStepResponseModel } from 'talenthive-models/lib/Entities';
import { ApplicationResponse } from '@Modules/application/dto/application.response';

//Responses
import { UserResponse } from '@/modules/user/dto/user.response';

@ObjectType()
export class ApplicationStepResponse
    extends Document
    implements ApplicationStepResponseModel
{
    @Field()
    _id: string;
    @Field(() => ApplicationResponse)
    application_id: ApplicationResponse;
    @Field()
    step: Number;
    @Field()
    name: String;
    @Field()
    description: String;
    @Field()
    duration: Number;
    @Field(() => [UserResponse])
    users?: UserResponse[] | Schema.Types.ObjectId[];
    @Field()
    @IsDate()
    createdAt: Date;
    @Field()
    @IsDate()
    updatedAt: Date;
}
