import { Field, InputType } from '@nestjs/graphql';
import { ObjectId, Schema } from 'mongoose';

import { ApplicationStepInputModel } from 'talenthive-models/lib/Entities';

@InputType()
export default class ApplicationStepInput implements ApplicationStepInputModel {
    @Field(() => String)
    application_id: ObjectId;
    @Field(() => Number)
    step: Number;
    @Field(() => Date)
    name: String;
    @Field(() => String)
    description?: String;
    @Field(() => Number)
    duration: Number;
    @Field(() => [String])
    users?: string[];
}
