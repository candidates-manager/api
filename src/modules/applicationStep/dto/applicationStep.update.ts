import { Field, InputType } from '@nestjs/graphql';
import { IsNumber, IsOptional, IsUrl } from 'class-validator';
import { Schema } from 'mongoose';

//Entities
import { ApplicationStepUpdateModel } from 'talenthive-models/lib/Entities';

@InputType()
export class ApplicationStepUpdate implements ApplicationStepUpdateModel {
    @Field(() => Number)
    step?: Number;
    @Field(() => Date)
    name?: String;
    @Field(() => String)
    description?: String;
    @Field(() => Number)
    duration?: Number;
    @Field(() => [Schema.Types.ObjectId])
    users?: Schema.Types.ObjectId[];
}
