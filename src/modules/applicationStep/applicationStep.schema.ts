import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { HydratedDocument, Types, ObjectId } from 'mongoose';

//Schemas
import { User } from '../user/user.schema';
import { Application } from '../application/application.schema';

//Entity
import {
    ApplicationEntity,
    ApplicationReviewEntity,
    ApplicationStepEntity,
} from 'talenthive-models/lib/Entities';
import { Application_Review } from '../applicationReview/ApplicationReview.schema';

@Schema()
export class Application_Step implements ApplicationStepEntity {
    _id: ObjectId;
    @Prop({
        type: Types.ObjectId,
        required: [true, 'An Application Id is required !'],
        unique: false,
        ref: 'Application',
    })
    application_id: ApplicationEntity;
    @Prop({
        required: true,
        type: Number,
    })
    step: Number;
    @Prop({
        required: true,
        type: String,
    })
    name: String;
    @Prop({
        required: false,
        type: String,
    })
    description: String;
    @Prop({
        required: true,
        type: Number,
    })
    duration: Number;
    @Prop({
        type: [Types.ObjectId],
        unique: false,
        ref: 'Application_Review',
    })
    reviews: ApplicationReviewEntity[];
    @Prop({
        type: [Types.ObjectId],
        unique: false,
        ref: 'User',
    })
    users?: User[];
    @Prop({
        default: new Date(),
        required: false,
    })
    updatedAt: Date;
    @Prop({
        default: new Date(),
        required: false,
    })
    createdAt: Date;
}
export const ApplicationStepSchema =
    SchemaFactory.createForClass(Application_Step);
//Step must be unique with the combination of one application_id and one step
ApplicationStepSchema.index({ application_id: 1, step: 1 }, { unique: true });
export type ApplicationStepDocument = HydratedDocument<Application_Step>;
