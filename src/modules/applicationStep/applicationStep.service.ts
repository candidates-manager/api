import { Injectable } from '@nestjs/common';

//Mongoose
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';

//Schema
import {
    Application_Step,
    ApplicationStepDocument,
} from './ApplicationStep.schema';

//DTO
import ApplicationStepCreateDto from './dto/ApplicationStep.input.dto';
import { ApplicationStepUpdate } from './dto/ApplicationStep.update';

//Response
import { ApplicationStepResponse } from './dto/ApplicationStep.response';

//Handle Error
import handleError from '@/Application/errors/handleError';
@Injectable()
export class ApplicationStepServices {
    constructor(
        @InjectModel(Application_Step.name)
        private ApplicationStepModel: Model<Application_Step>
    ) {}

    //Add ApplicationStep
    async addApplicationStep(
        data: ApplicationStepCreateDto
    ): Promise<ApplicationStepDocument | Error> {
        try {
            const newApplicationStep = <ApplicationStepDocument>(
                await this.ApplicationStepModel.create(data)
            );

            return newApplicationStep;
        } catch (error: unknown) {
            handleError(error, 'Cannot Add ApplicationStep');
        }
    }

    //Add ApplicationStep
    async addApplicationSteps(
        data: ApplicationStepCreateDto[]
    ): Promise<ApplicationStepDocument[] | Error> {
        try {
            const newApplicationSteps =
                await this.ApplicationStepModel.insertMany(data);

            return newApplicationSteps;
        } catch (error: unknown) {
            handleError(error, 'Cannot Add ApplicationStep');
        }
    }

    //Find ApplicationSteps
    async getApplicationSteps(
        params,
        projections = null,
        options = {}
    ): Promise<ApplicationStepResponse[] | Error> {
        try {
            const query = this.ApplicationStepModel.find(params, projections);
            query.populate('users', 'firstname lastname avatar');
            const response = <ApplicationStepResponse[]>await query.lean();
            return response;
        } catch (error: unknown) {
            handleError(error, 'Cannot Find Application Steps');
        }
    }

    //Get ApplicationStep
    async getApplicationStep(
        params,
        projections = null,
        options = {}
    ): Promise<ApplicationStepResponse | Error> {
        try {
            const query = this.ApplicationStepModel.findOne(
                params,
                projections
            );

            const response = <ApplicationStepResponse>await query.lean();

            return response;
        } catch (error: unknown) {
            handleError(error, 'Cannot Find ApplicationStep');
        }
    }

    //Update ApplicationStep
    async updateApplicationStep(
        _id: string,
        data: ApplicationStepUpdate
    ): Promise<ApplicationStepResponse | Error> {
        try {
            const updatedApplicationStep = <ApplicationStepResponse>(
                await this.ApplicationStepModel.findByIdAndUpdate(
                    { _id },
                    data,
                    {
                        new: true,
                    }
                )
            );

            return updatedApplicationStep;
        } catch (error: unknown) {
            handleError(error, 'Cannot Update ApplicationStep');
        }
    }

    //Delete ApplicationStep
    async deleteApplicationStep(_id: string): Promise<boolean | Error> {
        try {
            const response = await this.ApplicationStepModel.findByIdAndRemove(
                _id
            );
            if (response) return true;
            else return false;
        } catch (error: unknown) {
            handleError(error, 'Cannot Delete Application Step');
        }
    }
}
