import {
    Controller,
    Body,
    Get,
    Post,
    UseGuards,
    Param,
    Patch,
    Delete,
} from '@nestjs/common';

//Services
import { ApplicationStepServices } from './applicationStep.service';

//Handle Error
import handleError from '@/Application/errors/handleError';

//Restrictions
import {
    CompanyRestriction,
    CandidateRestriction,
} from '@/Application/decorators';

//DTO
import { ApplicationStepResponse } from './dto/ApplicationStep.response';
import { ApplicationStepUpdate } from './dto/ApplicationStep.update';

//Guards
import { Roles } from '@/Application/decorators/roles.decorator';

//Pipes
import { ValidateBody, ValidateParamId } from '@/Application/pipes';
import { ApplicationStepDocument } from './applicationStep.schema';
import { ValidateBodyApplicationStep } from '@/application/pipes/body/applicationStep.body.pipe';

//Controller
@Controller('applications/steps')
export default class ApplicationStepController {
    constructor(private applicationStepServices: ApplicationStepServices) {}
    //Add Application Step
    @Post('/add')
    @Roles('recruiter')
    @CandidateRestriction(true)
    async add(
        @Body(new ValidateBody()) body
    ): Promise<ApplicationStepDocument | Error> {
        try {
            const newStep = <ApplicationStepDocument>(
                await this.applicationStepServices.addApplicationStep(body)
            );
            return newStep;
        } catch (error) {
            handleError(error, 'Cannot Add ApplicationStep');
        }
    }

    //Get ApplicationStep
    @Get('/v/:_id')
    @Roles('recruiter', 'candidate')
    @CompanyRestriction(true)
    @CandidateRestriction(true)
    async get(
        @Param('ApplicationStep_id', ValidateParamId)
        ApplicationStep_id: string
    ): Promise<ApplicationStepResponse | Error> {
        try {
            const ApplicationStep = <ApplicationStepResponse>(
                await this.applicationStepServices.getApplicationStep({
                    _id: ApplicationStep_id,
                })
            );
            return ApplicationStep;
        } catch (error) {
            handleError(error, 'Cannot Get ApplicationStep');
        }
    }

    //Update ApplicationStep
    @Patch('/u/:_id')
    @Roles('recruiter')
    @CompanyRestriction(true)
    async update(
        @Body(new ValidateBodyApplicationStep())
        body: ApplicationStepUpdate,
        @Param('_id', ValidateParamId)
        _id: string
    ): Promise<ApplicationStepResponse | Error> {
        try {
            const ApplicationStep = <ApplicationStepResponse>(
                await this.applicationStepServices.updateApplicationStep(
                    _id,
                    body
                )
            );
            return ApplicationStep;
        } catch (error) {
            handleError(error, 'Cannot Update ApplicationStep');
        }
    }

    //Delete Application Process
    @Delete('/d/:_id')
    @Roles('recruiter')
    @CompanyRestriction(true)
    async delete(
        @Param('_id', ValidateParamId)
        _id: string
    ): Promise<boolean | Error> {
        try {
            return await this.applicationStepServices.deleteApplicationStep(
                _id
            );
        } catch (error) {
            handleError(error, 'Cannot Delete Job Process');
        }
    }
}
