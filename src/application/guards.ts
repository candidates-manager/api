import { CompanyGuard } from './guards/company.guard';
import { RolesGuard } from './guards/role.guard';
import { AuthJWTGuard } from './guards/auth.guard';
import { CandidateGuard } from './guards/candidate.guard';
import { OnlyMeGuard } from './guards/onlyMe.guard';

export { OnlyMeGuard, CompanyGuard, RolesGuard, AuthJWTGuard, CandidateGuard };
