import { SetMetadata } from '@nestjs/common';

export const CompanyRestriction = (CompanyRestriction: boolean) =>
    SetMetadata('CompanyRestriction', CompanyRestriction);
