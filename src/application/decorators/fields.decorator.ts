import { createParamDecorator, ExecutionContext } from '@nestjs/common';
import { GqlExecutionContext } from '@nestjs/graphql';

const getFields = createParamDecorator(
    (data: unknown, context: ExecutionContext) => {
        const ctx = GqlExecutionContext.create(context);
        const info = ctx.getInfo();
        const fields = [];
        if (info?.fieldNodes?.[0]?.selectionSet?.selections) {
            info.fieldNodes[0].selectionSet.selections.map((item) => {
                fields.push(item.name.value);
            });
        }
        return fields;
    }
);

export default getFields;
