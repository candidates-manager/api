import { SetMetadata } from '@nestjs/common';

export const CandidateRestriction = (candidateRestriction: boolean) =>
    SetMetadata('CandidateRestriction', candidateRestriction);
