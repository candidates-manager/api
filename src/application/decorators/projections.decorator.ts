import { createParamDecorator, ExecutionContext } from '@nestjs/common';
import { GqlExecutionContext } from '@nestjs/graphql';

export const Projections = createParamDecorator(
    (data: unknown, context: ExecutionContext) => {
        const ctx = GqlExecutionContext.create(context);
        const info = ctx.getInfo();
        const projections = {};
        // if (info?.fieldNodes?.[0]?.selectionSet?.selections) {
        //   info.fieldNodes[0].selectionSet.selections.map((item) => {
        //     projection[item.name.value] = 1;
        //   });
        // }
        return projections;
    }
);
