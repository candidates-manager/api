import { SetMetadata } from '@nestjs/common';

export const AuthRestriction = (authrestriction: boolean) =>
    SetMetadata('AuthRestriction', authrestriction);
