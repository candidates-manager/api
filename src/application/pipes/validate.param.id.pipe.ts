import { HttpStatus, Injectable, PipeTransform } from '@nestjs/common';
import { CustomHttpException } from '../errors/customHttpException';
import mongoose, { Types } from 'mongoose';
import toObjectId from '@Functions/toObjectId';

@Injectable()
export class ValidateParamId implements PipeTransform<Types.ObjectId | string> {
    transform(value: string) {
        if (!value) return null;
        const newValue = toObjectId(value);
        const isValidObjectId = mongoose.isValidObjectId(newValue);

        if (!isValidObjectId) {
            throw new CustomHttpException({
                message: 'Value ID is required or incorrect',
                code: 'validation/invalid-id',
                statusCode: HttpStatus.BAD_REQUEST,
            });
        }
        return newValue;
    }
}
