import { Injectable, PipeTransform } from '@nestjs/common';
import toObjectId from '../functions/toObjectId';

@Injectable()
export class ValidateArgs implements PipeTransform<string, string> {
    transform(value: any) {
        //Id
        if (value._id) {
            value._id = toObjectId(value._id);
        }

        //Company Id
        if (value.company_id) {
            value.company_id = toObjectId(value.company_id);
            //Array
            if (value.company_id instanceof Array) {
                value.company_id = {
                    $in: value.company_id,
                };
            }
        }

        return value;
    }
}
