import { Injectable, PipeTransform } from '@nestjs/common';
import toObjectId from '../../functions/toObjectId';

@Injectable()
export class ValidationBodyApplication
    implements PipeTransform<string, string>
{
    transform(value: any) {
        // const rating = {
        //     experience: value.rating_experience ? value.rating_experience : '',
        //     skills: value.rating_skills ? value.rating_skills : '',
        //     softskills: value.rating_softskills ? value.rating_softskills : '',
        // };

        // value.rating = rating;

        //Start
        // if (value.startAt) {
        //     let [day, month, year] = value.startAt.split('/');
        //     value.startAt = new Date(+year, +month - 1, +day);
        // }

        return value;
    }
}
