import { Injectable, PipeTransform } from '@nestjs/common';
import toObjectId from '../../functions/toObjectId';

@Injectable()
export class ValidateBodyApplicationStep
    implements PipeTransform<string, string>
{
    transform(value: any) {
        if (value._id) {
            value._id = toObjectId(value._id);
        }

        if (value.users) {
            value.users = toObjectId(value.users);
        }

        return value;
    }
}
