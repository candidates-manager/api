import { Injectable, PipeTransform } from '@nestjs/common';
import toObjectId from '../../functions/toObjectId';

@Injectable()
export class ValidateBody implements PipeTransform<string, string> {
    transform(value: any) {
        //Id
        if (value._id) {
            value._id = toObjectId(value._id);
        }

        //Company Id
        if (value.company_id) {
            value.company_id = toObjectId(value.company_id);
        }

        //Application Process Id
        if (value.application_process_id) {
            value.application_process_id = toObjectId(
                value.application_process_id
            );
        }

        //Application  Id
        if (value.application_id) {
            value.application_id = toObjectId(value.application_id);
        }

        //User Id
        if (value.user_id) {
            value.user_id = toObjectId(value.user_id);
        }

        //Entity Id
        if (value.entity_id) {
            value.entity_id = toObjectId(value.entity_id);
        }

        value.updatedAt = new Date();

        return value;
    }
}
