import { Injectable, PipeTransform } from '@nestjs/common';
import toObjectId from '../../functions/toObjectId';

@Injectable()
export class ValidationBodyApplicationReview
    implements PipeTransform<string, string>
{
    transform(value: any) {
        //Application ID
        if (value.application_id) {
            value.application_id = toObjectId(value.application_id);
        }

        //Step Id
        if (value.step_id) {
            value.step_id = toObjectId(value.step_id);
        }

        //User Id
        if (value.user_id) {
            value.user_id = toObjectId(value.user_id);
        }

        return value;
    }
}
