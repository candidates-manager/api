import { Injectable, PipeTransform } from '@nestjs/common';
import toObjectId from '@Functions/toObjectId';

@Injectable()
export class ValidateCompanyBody implements PipeTransform<string, string> {
    transform(value: any) {
        //Id
        if (value._id) {
            value._id = toObjectId(value._id);
        }

        value.urls = {
            linkedin: value.linkedin,
            glassdoor: value.glassdoor,
        };

        value.updatedAt = new Date();

        return value;
    }
}
