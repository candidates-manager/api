import { Injectable, PipeTransform } from '@nestjs/common';
import toObjectId from '@Functions/toObjectId';

@Injectable()
export class ValidateUserBody implements PipeTransform<string, string> {
    transform(value: any) {
        //Id
        if (value._id) {
            value._id = toObjectId(value._id);
        }

        //Role
        if (value.role) {
            value.role = [value.role];
        }

        //Urls
        if (!value.urls) {
            value.urls = {};
        }

        //Linkedin
        if (value.linkedin) {
            value.urls.linkedin = value.linkedin;
            delete value.linkedin;
        }

        //Company
        if (value.company) {
            value.company = [value.company];
        }

        //Fullname
        if (value.firstname && value.lastname) {
            value.fullname = value.firstname + ' ' + value.lastname;
        }

        value.updatedAt = new Date();

        return value;
    }
}
