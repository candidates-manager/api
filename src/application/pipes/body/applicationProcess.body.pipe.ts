import { Injectable, PipeTransform } from '@nestjs/common';
import toObjectId from '@Functions/toObjectId';
import { ApplicationStepInputModel } from 'talenthive-models/lib/Entities';
import { Types } from 'mongoose';

@Injectable()
export class ValidateBodyApplicationProcess
    implements PipeTransform<string, string>
{
    transform(value: any) {
        //Id
        if (value._id) {
            value._id = toObjectId(value._id);
        }

        //Steps
        if (value.steps) {
            for (let i = 0; i < value.steps.length; i++) {
                value.steps.map((s: ApplicationStepInputModel) => {
                    s.users = <Types.ObjectId[]>toObjectId(<string[]>s.users);
                });
            }
        }

        value.updatedAt = new Date();

        return value;
    }
}
