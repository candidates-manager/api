import {
    CallHandler,
    ExecutionContext,
    Injectable,
    NestInterceptor,
} from '@nestjs/common';
import { GqlExecutionContext, GqlContextType } from '@nestjs/graphql';
import { map, Observable } from 'rxjs';

interface ResponseData {
    success: boolean;
    statusCode: number;
    status: string;
    data?: any;
}

@Injectable()
export default class FormatResponseInterceptor implements NestInterceptor {
    intercept(context: ExecutionContext, next: CallHandler): Observable<any> {
        const contextType = context.getType<GqlContextType>();

        //Graphql
        if (contextType === 'graphql') {
            const gqlContext = GqlExecutionContext.create(context);
            const response: Response = gqlContext.getContext().res;
            return next.handle();
        }

        //Rest API
        else if (contextType === 'http') {
            const ctx = context.switchToHttp();
            const response = ctx.getResponse();
            return next.handle().pipe(
                map((value) => {
                    const data: ResponseData = {
                        success: true,
                        status: 'OK',
                        statusCode: response.statusCode,
                    };
                    value ? (data.data = value) : null;
                    return data;
                })
            );
        }

        return next.handle();
    }
}
