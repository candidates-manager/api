import { HttpException } from '@nestjs/common';

export interface CustomError {
    message: string;
    code: string;
    fields?: string[];
    statusCode: number;
}
export class CustomHttpException extends HttpException {
    constructor(obj: CustomError, status?: number) {
        super(obj, obj.statusCode);
    }
}
