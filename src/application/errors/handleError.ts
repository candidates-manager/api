import { HttpException, HttpStatus } from '@nestjs/common';
import mongoose from 'mongoose';

import {
    CustomHttpException,
    CustomError,
} from '@Application/errors/customHttpException';

const handleError = (
    error: any | CustomError,
    defaultMessage: string = 'Something wrong happened !'
): Error | any => {
    //Log
    //TO DO - INSTALL A WRITING LOG
    if (process.env.NODE_ENV === 'development') {
        console.log(
            error.name,
            typeof error.constructor,
            error.constructor,
            error.message
        );
    }

    switch (error.name) {
        //TO DO - FIND A WAY TO RETURN AN ERROR WITHOUT CRASHING THE SERVER
        case 'MongoServerError' || 'BSONTypeError':
            const fields = error.errmsg
                .split('index:')[1]
                .split('dup key')[0]
                .split('_')[0];
            throw new CustomHttpException({
                message: 'You have one duplicate field.',
                fields: [fields],
                code: 'validation/duplicate-fields',
                statusCode: HttpStatus.BAD_REQUEST,
            });
        //Validation Error
        case 'ValidationError':
            throw new CustomHttpException({
                message: error.message,
                code: 'validation/validation-error',
                statusCode: HttpStatus.FORBIDDEN,
            });
        //Custom Http Exception
        case 'CustomHttpException':
            const response = <CustomError>error.getResponse();
            throw new CustomHttpException(response);
        //Http Exception
        case 'HttpException':
            throw new HttpException(error.message, error.getStatus());
        default:
            throw new HttpException(defaultMessage, HttpStatus.BAD_REQUEST);
    }
};

export default handleError;
