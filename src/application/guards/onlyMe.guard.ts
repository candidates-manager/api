import { Injectable, ExecutionContext, HttpStatus } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { AuthGuard } from '@nestjs/passport';
import { GqlExecutionContext } from '@nestjs/graphql';
import { ExecutionContextHost } from '@nestjs/core/helpers/execution-context-host';
//Handle Error
import { CustomHttpException } from '@/application/errors/customHttpException';
@Injectable()
export class OnlyMeGuard extends AuthGuard('jwt') {
    private user_id: string = null;
    private restriction: boolean = false;
    constructor(private reflector: Reflector) {
        super();
        this.restriction = false;
    }
    canActivate(context: ExecutionContext) {
        const ctx = GqlExecutionContext.create(context);
        const { req } = ctx.getContext();

        //Get Restriction
        this.restriction = this.reflector.get<boolean>(
            'OnlyMe',
            context.getHandler()
        );

        this.user_id = req.params.user_id;
        return super.canActivate(new ExecutionContextHost([req]));
    }
    handleRequest(err: any, user: any, info: string) {
        if (!this.restriction) return user;

        if (!this.user_id || this.user_id !== user.user_id) {
            throw new CustomHttpException({
                message: 'This action is forbbiden.',
                code: 'auth/forbbiden',
                statusCode: HttpStatus.FORBIDDEN,
            });
        }
        return user;
    }
}
