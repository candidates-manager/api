import { Injectable, ExecutionContext, HttpStatus } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { AuthGuard } from '@nestjs/passport';

//Entities
import JobResponseDocument from '@/modules/job/dto/job.response';
import { ApplicationResponse } from '@/modules/application/dto/application.response';
import { ApplicationProcessResponse } from '@Modules/applicationProcess/dto/applicationProcess.response';

//Handle Error
import { CustomHttpException } from '@/application/errors/customHttpException';

//Functions
import getContextInfo from '@Functions/getContextInfo';
import toObjectId from '../functions/toObjectId';

//Services
import JobServices from '@/modules/job/job.service';
import ApplicationServices from '@/modules/application/application.service';
import ApplicationProcessServices from '@/modules/applicationProcess/applicationProcess.service';

@Injectable()
export class CompanyGuard extends AuthGuard('jwt') {
    constructor(
        private readonly applicationServices: ApplicationServices,
        private readonly applicationProcessServices: ApplicationProcessServices,
        private readonly jobServices: JobServices,
        private reflector: Reflector
    ) {
        super();
    }
    async canActivate(context: ExecutionContext): Promise<any> {
        const { args, req } = getContextInfo(context);
        const user = req.user;
        let job_id = req.params.job_id || '';
        const application_id = req.params.application_id || '';
        const application_process_id = req.params.application_process_id || '';
        let companies: string[];

        //Get Restriction
        const restriction = this.reflector.get<boolean>(
            'CompanyRestriction',
            context.getHandler()
        );
        //No restriction or no company restriction mentionned or User is an admin
        if (!restriction || user.roles.includes('admin')) {
            return true;
        }

        //Admin : No Restriction
        if (user.roles.includes('admin')) {
            return true;
        }

        //This restriction is only for Recruiters and Employees
        if (
            !user.roles.includes('recruiter') &&
            !user.roles.includes('employee')
        ) {
            return true;
        }

        //Company Id
        if (args?.company_id !== undefined && args?.company_id.length > 0) {
            companies = args.company_id;
        }

        //Application Id
        if (application_id) {
            const findApplication = <ApplicationResponse>(
                await this.applicationServices.getApplication({
                    _id: toObjectId(application_id),
                })
            );

            if (findApplication) {
                job_id = findApplication.job_id;
            }
        }

        //Application Process Id
        if (application_process_id) {
            const findApplicationProcess = <ApplicationProcessResponse>(
                await this.applicationProcessServices.getApplicationProcess({
                    _id: toObjectId(application_process_id),
                })
            );

            if (findApplicationProcess) {
                const applicationprocessCompanies =
                    findApplicationProcess.company_id.map((c) => c._id);
                companies.push(...applicationprocessCompanies);
            }
        }

        //Request with a company Id
        if (companies && companies.length > 0) {
            const commonCompany = user.companies.filter((value) =>
                companies.includes(value)
            );
            if (commonCompany.length > 0) return true;
        }

        //Job Id
        if (job_id) {
            const findJob = <JobResponseDocument>await this.jobServices.getJob(
                {
                    _id: toObjectId(job_id),
                    company_id: { $in: toObjectId(req.user.companies) },
                },
                {},
                {}
            );

            if (findJob) return true;
        }

        return false;
    }

    handleRequest(err: any, user: any, info: string) {
        return user;
    }
}
