import { Injectable, ExecutionContext } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { AuthGuard } from '@nestjs/passport';

//Entities
import JobResponseDocument from '@/modules/job/dto/job.response';
import { ApplicationResponse } from '@/modules/application/dto/application.response';

//Functions
import getContextInfo from '@Functions/getContextInfo';
import toObjectId from '../functions/toObjectId';

//Services
import JobServices from '@/modules/job/job.service';
import ApplicationServices from '@/modules/application/application.service';

@Injectable()
export class CandidateGuard extends AuthGuard('jwt') {
    constructor(
        private readonly jobServices: JobServices,
        private readonly applicationServices: ApplicationServices,
        private reflector: Reflector
    ) {
        super();
    }
    async canActivate(context: ExecutionContext): Promise<boolean> {
        const { args, req } = getContextInfo(context);
        const user = req.user;
        let job_id = req.params.job_id || '';
        let application_id = req.params.application_id || '';
        let candidate_id = req.params.candidate_id || '';

        //Get Restriction
        const restriction = this.reflector.get<boolean>(
            'CandidateRestriction',
            context.getHandler()
        );
        //No restriction or no candidate restriction mentionned or User is an admin
        if (!restriction || user.roles.includes('admin')) {
            return true;
        }

        //Admin : No Restriction
        if (user.roles.includes('admin')) {
            return true;
        }

        //This restriction is only for Candidates
        if (!user.roles.includes('candidate')) {
            return true;
        }

        //Candidate Id
        if (args?.candidate_id !== undefined) {
            candidate_id = args.candidate_id;
        }

        //Application
        if (application_id) {
            const findApplication = <ApplicationResponse>(
                await this.applicationServices.getApplication(
                    {
                        _id: toObjectId(application_id),
                        candidate_id: candidate_id,
                    },
                    {},
                    {}
                )
            );

            if (findApplication) job_id = findApplication.job_id;
        }

        //Job Id
        if (job_id) {
            const findJob = <JobResponseDocument>await this.jobServices.getJob(
                {
                    _id: toObjectId(job_id),
                    candidate_id: candidate_id,
                },
                {},
                {}
            );

            if (findJob) return true;
        }

        return false;
    }

    handleRequest(err: any, user: any, info: string) {
        return user;
    }
}
