import {
    HttpStatus,
    Injectable,
    ExecutionContext,
    UseGuards,
} from '@nestjs/common';

import { Reflector } from '@nestjs/core';
import { GqlThrottlerGuard } from './gqlThrottler.guard';
import { GqlExecutionContext } from '@nestjs/graphql';
import { AuthGuard } from '@nestjs/passport';

//Handle Error
import { CustomHttpException } from '@/application/errors/customHttpException';

@Injectable()
@UseGuards(GqlThrottlerGuard)
export class AuthJWTGuard extends AuthGuard('jwt') {
    private restrictAuth: boolean;

    constructor(private reflector: Reflector) {
        super();
        this.restrictAuth = true;
    }
    getRequest(context: ExecutionContext) {
        const ctx = GqlExecutionContext.create(context);
        this.restrictAuth = this.reflector.get<boolean>(
            'AuthRestriction',
            context.getHandler()
        );

        return ctx.getContext().req;
    }
    handleRequest(err, user, info) {
        //Check if it does not require an authentification
        if (!this.restrictAuth) return user;
        //Check the restriction
        if (err || !user) {
            throw new CustomHttpException({
                message: 'This action is forbbiden.',
                code: 'auth/forbbiden',
                statusCode: HttpStatus.FORBIDDEN,
            });
        } else {
            return user;
        }
    }
}
