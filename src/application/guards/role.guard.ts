import { Injectable, ExecutionContext, HttpStatus } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { AuthGuard } from '@nestjs/passport';
import { GqlExecutionContext } from '@nestjs/graphql';
import { ExecutionContextHost } from '@nestjs/core/helpers/execution-context-host';
//Handle Error
import { CustomHttpException } from '@/application/errors/customHttpException';
@Injectable()
export class RolesGuard extends AuthGuard('jwt') {
    private roles: string[] = [];

    constructor(private reflector: Reflector) {
        super();
        this.roles = [];
    }
    canActivate(context: ExecutionContext) {
        const ctx = GqlExecutionContext.create(context);
        const { req } = ctx.getContext();
        this.roles = this.reflector.get<string[]>(
            'roles',
            context.getHandler()
        );
        return super.canActivate(new ExecutionContextHost([req]));
    }

    handleRequest(err: any, user: any, info: string) {
        //No role restriction mentionned
        if (!this.roles) {
            return user;
        }

        //User has no roles
        if (!user.roles) {
            throw new CustomHttpException({
                message: 'This action is forbbiden.',
                code: 'role/forbbiden',
                statusCode: HttpStatus.FORBIDDEN,
            });
        }

        //User is an admin, let's pass the guard
        if (user.roles.includes('admin')) {
            return user;
        }

        //Array Intersection
        const commonRoles = user.roles.filter((value) =>
            this.roles.includes(value)
        );

        if (commonRoles.length === 0) {
            throw new CustomHttpException({
                message: 'This action is forbbiden.',
                code: 'role/forbbiden',
                statusCode: HttpStatus.FORBIDDEN,
            });
        }
        return user;
    }
}
