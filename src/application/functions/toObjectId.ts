import { Types } from 'mongoose';

const toObjectId = (
    data: string | string[] | string[]
): Types.ObjectId | Types.ObjectId[] => {
    //String
    if (typeof data === 'string') {
        return new Types.ObjectId(data);
    }
    //Array
    if (Array.isArray(data)) {
        return data.map((v) => new Types.ObjectId(v));
    } else return data;
};
export default toObjectId;
