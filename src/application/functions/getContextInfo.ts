import { GqlExecutionContext, GqlContextType } from '@nestjs/graphql';
import { ExecutionContext } from '@nestjs/common';

interface ContextInfo {
    args: any;
    req: any;
    params: any;
}

const getContextInfo = (context: ExecutionContext): ContextInfo => {
    const contextInfo = {
        args: null,
        params: null,
        req: null,
    };
    if (context.getType<GqlContextType>() === 'graphql') {
        const gqlContext = GqlExecutionContext.create(context);
        contextInfo.req = gqlContext.getContext().req;
        contextInfo.args = {
            ...gqlContext.getArgs(),
            $parent: gqlContext.getRoot(),
        };
    } else if (context.getType() === 'http') {
        const httpContext = context.switchToHttp();
        const request = httpContext.getRequest();
        contextInfo.req = request;
        contextInfo.params = request.params;
        contextInfo.args = request.body;
    }
    return contextInfo;
};
export default getContextInfo;
