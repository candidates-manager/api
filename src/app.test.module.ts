//Nestjs
import { Module, Scope } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { ApolloDriver, ApolloDriverConfig } from '@nestjs/apollo';
import { ThrottlerModule } from '@nestjs/throttler';

//Graphql
import { GraphQLModule } from '@nestjs/graphql';

//Others
import { join } from 'path';

//App
import AppResolver from './app.resolver';

//Auth
import AuthModule from '@Modules/auth/auth.module';
import UsersModule from '@Modules/user/user.module';

//Infrastructure
// import configuration from '@Infrastructure/config/configuration';

//Mongoose
import { MongooseModule } from '@nestjs/mongoose';

//Auth Strategy
import { LocalStrategy } from '@/modules/auth/strategies/auth.strategy.local';

@Module({
    imports: [
        ThrottlerModule.forRoot({
            ttl: 60,
            limit: 100,
        }),
        MongooseModule.forRoot(process.env.MONGO_DB_TEST_URL, {
            autoIndex: true,
        }),
        GraphQLModule.forRoot<ApolloDriverConfig>({
            driver: ApolloDriver,
            autoSchemaFile: join(process.cwd(), 'src/schema.gql'),
            sortSchema: true,
            playground: process.env.NODE_ENV !== 'production',
            context: ({ req }) => {
                return {
                    request: req,
                };
            },
        }),
        // ConfigModule.forRoot({ load: [configuration], isGlobal: true }),
        AuthModule,
        UsersModule,
    ],
    providers: [AppResolver, LocalStrategy],
})
export default class AppTestModule {}
