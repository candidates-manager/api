//Nestjs
import { APP_INTERCEPTOR, APP_GUARD } from '@nestjs/core';
import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { ApolloDriver, ApolloDriverConfig } from '@nestjs/apollo';
import { ThrottlerModule } from '@nestjs/throttler';

//Mongoose
import { MongooseModule } from '@nestjs/mongoose';

//Interceptors
import FormatResponseInterceptor from '@/application/interceptors/formatResponse.interceptor';

//Graphql
import { GraphQLModule } from '@nestjs/graphql';

//Others
import { join } from 'path';

//App
import AppResolver from './app.resolver';

//Modules
import AuthModule from '@Modules/auth/auth.module';
import UsersModule from '@Modules/user/user.module';
import UserRolesModule from '@Modules/userRole/role.module';
import CompanyModule from '@Modules/company/company.module';
import JobModule from '@Modules/job/job.module';
import ApplicationModule from '@Modules/application/application.module';
import ApplicationProcessModule from '@Modules/applicationProcess/applicationProcess.module';
import ApplicationStep from '@Modules/applicationStep/applicationStep.module';
import ApplicationReview from '@Modules/applicationReview/applicationReview.module';
import CandidateModule from '@Modules/candidate/candidate.module';
import ReplyModule from './modules/reply/reply.module';
//API
import MailerSendModule from '@/api/mail/mail.module';

//Auth Strategy
import { LocalStrategy } from '@/modules/auth/strategies/auth.strategy.local';

//Guards
import {
    AuthJWTGuard,
    RolesGuard,
    CompanyGuard,
    CandidateGuard,
    OnlyMeGuard,
} from './application/guards';

@Module({
    imports: [
        ConfigModule.forRoot({
            isGlobal: true,
            envFilePath: '.env',
        }),
        MongooseModule.forRoot(process.env.MONGO_DB_URL, {
            autoIndex: true,
        }),
        ThrottlerModule.forRoot({
            ttl: 60,
            limit: 100,
        }),
        GraphQLModule.forRoot<ApolloDriverConfig>({
            driver: ApolloDriver,
            autoSchemaFile: join(process.cwd(), 'src/schema.gql'),
            sortSchema: true,
            playground: process.env.NODE_ENV !== 'production',
            // cors: {
            //     origin:
            //         process.env.CORS_GQL !== 'undefined' ??
            //         process.env.CORS_GQL.split(', '),
            //     credentials: true,
            // },
            formatError: (error) => {
                if (process.env.NODE_ENV === 'production')
                    return { message: error.message };
                return error;
            },
        }),
        AuthModule,
        UsersModule,
        CompanyModule,
        JobModule,
        CandidateModule,
        MailerSendModule,
        ApplicationModule,
        ApplicationStep,
        ApplicationReview,
        ApplicationProcessModule,
        UserRolesModule,
        ReplyModule,
    ],
    providers: [
        AppResolver,
        LocalStrategy,
        {
            provide: APP_INTERCEPTOR,
            useClass: FormatResponseInterceptor,
        },
        {
            provide: APP_GUARD,
            useClass: AuthJWTGuard,
        },
        {
            provide: APP_GUARD,
            useClass: RolesGuard,
        },
        {
            provide: APP_GUARD,
            useClass: CompanyGuard,
        },
        {
            provide: APP_GUARD,
            useClass: CandidateGuard,
        },
        {
            provide: APP_GUARD,
            useClass: OnlyMeGuard,
        },
    ],
})
export class AppModule {}
