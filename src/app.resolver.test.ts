import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import mongoose from 'mongoose';

//Module Tested
import MediaResolver from './app.resolver';

//App Test
import AppTest from './app.test.module';

describe('MediaResolver', () => {
  let app: INestApplication;
  let resolver: MediaResolver;
  let data;

  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [AppTest],
      providers: [MediaResolver],
    }).compile();
    app = module.createNestApplication();
    await app.init();
    resolver = module.get<MediaResolver>(MediaResolver);

    data = await resolver.sayHello();
  });

  afterAll(async () => {
    mongoose.connection.close();
    await app.close();
  });

  it('Components should be defined', () => {
    expect(app).toBeDefined();
    expect(resolver).toBeDefined();
  });

  test('My App should say Hello !', async () => {
    expect(data).not.toBeNull();
    expect(typeof data).toBe('string');
    expect(data).toMatch('Hello World');
  });
});
